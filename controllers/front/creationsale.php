<?php

use Modules\Itou\Itou\Module\Utils;
use Modules\Itou\Itou\Module\Upload;
/**
 * Itou
 * Form to create a sale
 */
class itoucreationsaleModuleFrontController extends ModuleFrontController
{
	protected $prefix_reference = 'itou';
	protected $path_script = "itou/node_modules/";
	protected $url_upload = "index.php?fc=module&module=itou&controller=creationsale&uploadFile=1";
	protected $tax = 20;

	/**
	 * Construct
	 */
	public function __construct()
	{
		parent::__construct(); 
	}

	 /**
     * Add CSS and JS
     */
	public function setMedia()
	{
		parent::setMedia();
		$this->addJS(_MODULE_DIR_ . $this->path_script . 'fancybox/dist/js/jquery.fancybox.js');
        $this->addJS(_MODULE_DIR_ . $this->path_script . 'dropzone/dist/min/dropzone.min.js');
		$this->addCSS(_MODULE_DIR_ . $this->path_script . 'dropzone/dist/min/dropzone.min.css');
		$this->addCSS(_MODULE_DIR_ . $this->path_script . 'fancybox/dist/css/jquery.fancybox.css');
		$this->addJS(_MODULE_DIR_ . 'itou/views/js/search-buyer.js');
		$this->addJS(_MODULE_DIR_ . 'itou/views/js/my-dropzone.js');
		$this->addJS(_MODULE_DIR_ . 'itou/views/js/my-fancybox.js');
		$this->addJS(_MODULE_DIR_ . 'itou/views/js/global.js');
	}
	
	/**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$context = Context::getContext();
		$this->assignVariables();

		if ($context->customer->logged === true) {
			$this->manageContent($context);
		}else{
			Tools::redirect('authentication');
		}
	}

	/**
	 * Manage content to display or action to launch
	 */
	public function manageContent($context)
	{
		$tpl = 'creation.tpl';
		$id_lang = $context->language->id;
		$display_link_subscription = true;

		if (Tools::getValue('submit')) {

			if(Tools::getIsset('product_id')) {
				$this->saveProduct($context, Tools::getValue('product_id'));
			}else{
				$this->saveProduct($context);
			}

		} elseif(Tools::getValue('editProduct') && Tools::getIsset('product_id')) {

			$product_id = Tools::getValue('product_id');
			$product = new Product($product_id, false, $id_lang);
			$product->price = $product->price;
			$productExtraField = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($product_id);

			if($productExtraField->is_cancelled)
				Tools::redirect('my-account');
				
			$customerExtraField = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($productExtraField->buyer_id);
		

			$context->smarty->assign(
				array(
					'product' => $product,
					'is_shipping_cost_paid' => $productExtraField->is_shipping_cost_paid,
					'buyer_login' => $customerExtraField->login,
					'buyer_id' => $customerExtraField->id
					)
			);
			$tpl = 'edit.tpl';
			$context->cookie->__set("productIdEdited", $product_id);

		} elseif(Tools::getValue('uploadFile')) {

			Upload::uploadImage($context);

		} elseif(Tools::getValue('removeFile')) {

			if(Tools::getIsset('image_id'))
				Upload::removeUploadedImage($context, Tools::getValue('image_id'));
			else
				Upload::removeUploadedImage($context);

		} elseif(Tools::getValue('getFiles')) {

			Upload::getUploadedImages(null, $context);

		}elseif(Tools::getValue('saveBankFields')) {
			$this->fillCustomerExtraFields($context->customer->id, Tools::getValue('iban'), Tools::getValue('bic'));
		}

		$subscription = new \Modules\Itou\Subscription\Subscription($context->customer->id);
		if($subscription && $subscription->subscription_id != Configuration::get('SUBSCRIPTION_SIMPLE_ID')) {
			$display_link_subscription = false;
		}

		$this->context->smarty->assign(array(
			'url_upload' => $this->url_upload,
			'display_link_subscription' => $display_link_subscription));

		$this->setTemplate('module:itou/views/templates/front/my-account/forms/'.$tpl);
	}

	/**
	 * Create Product from form front
	 * @param $context
	 */
	public function saveProduct(Context $context, $product_id = null)
	{
		$languages = Language::getLanguages();
		$link = new Link();

		if(is_null($product_id)) {
			$product = new Product();
			$product->reference = $this->prefix_reference . Utils::randomRef();
		}else{
			$product = new Product($product_id);
		}
		
		foreach ($languages as $lang) {
			$product->name[$lang['id_lang']] = Tools::getValue('name');
			$product->link_rewrite[$lang['id_lang']] = Tools::link_rewrite(Tools::getValue('name'));
		}

		$product->price = (float)Tools::getValue('price');
		$product->weight = (float) Tools::getValue('weight');
		$product->active = true;
		$product->available_for_order = true;
		$product->indexed = false;
		$product->id_category_default = Configuration::get('PS_HOME_CATEGORY');
		
		$idRelaisCurrentCustomer = !empty($context->cookie->idRelaisCurrentCustomer) ? $context->cookie->idRelaisCurrentCustomer : null;

		if ($idRelaisCurrentCustomer == null) {
			$this->success[] = $this->l('Vous devez choisir un point relais de depot pour votre colis.');
			$this->redirectWithNotifications($link->getModuleLink('itou','creationsale'));
			die();
		}

		if($product->save()) {

			

			$this->fillProductExtraFields($product->id, $context->customer->id, Tools::getValue('buyer_id'), Tools::getValue('is_shipping_cost_paid'), $idRelaisCurrentCustomer);
			
			StockAvailable::setQuantity($product->id, 0, (int) 1);
			$product->addToCategories(array(Configuration::get('PS_HOME_CATEGORY')));
			$this->initNewCart();
		}
		
		Upload::saveImageToProduct($context, $product->id);

		$this->success[] = $this->l('Votre transaction est sur le point d\'être validée. Cliquez sur le bouton bleu ci-dessous « envoyer ».Votre acheteur sera informé pour payer sa transaction. À votre tour vous recevrez un SMS et MAIL pour imprimer votre étiquette. Vous disposerez alors de 5 jours ouvrés pour déposer votre colis.');
		$this->redirectWithNotifications($link->getModuleLink('itou','sales'));

	}

	/**
	 * Module productextrafields
	 * @param int $product_id
	 * @param int $seller_id
	 * @param int $buyer_id
	 */
	public function fillProductExtraFields(int $product_id, int $seller_id, $buyer_id = null, $is_shipping_cost_paid = false, int $seller_relais_id = null) 
	{
		$productExtraField = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($product_id);
		$productExtraField->seller_id = $seller_id;
		$productExtraField->is_shipping_cost_paid = $is_shipping_cost_paid;
		$productExtraField->seller_relais_id = !is_null($seller_relais_id) ? $seller_relais_id : $productExtraField->seller_relais_id;
		$productExtraField->date_send = null;
		$productExtraField->buyer_id = null;

		if(!is_null($buyer_id)){
			$productExtraField->buyer_id = $buyer_id;
		}

		if(!is_null($buyer_id)){
			$productExtraField->buyer_id = $buyer_id;
		}

		$subscription_customer = Utils::checkField('subscription_id', 'subscription', 'customer_id', $this->context->customer->id, true);
		$productExtraField->save(true, true);
	}

		/**
	 * Module customerextrafields
	 * @param int $customer_id
	 * @param $bic
	 * @param $iban
	 */
	public function fillCustomerExtraFields(int $customer_id, $iban = '', $bic = '') 
	{
		
		$customerExtraField = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($customer_id);
		$customerExtraField->iban = $iban;
		$customerExtraField->bic_code = $bic;

		if($customerExtraField->save(true, true)){
			if(!empty($iban) && !empty($bic)){
				return ['iban' => $iban, 'bic' => $bic];
			}
		}
		return false;
	}

	 /**
     * Assign variable in creation or update page
     */
	public function assignVariables()
	{
        $has_iban = Utils::checkField('iban', 'customerextrafields', 'id', Context::getContext()->customer->id, true);
		$has_bic = Utils::checkField('bic_code', 'customerextrafields', 'id', Context::getContext()->customer->id, true);
		$has_subscription = Utils::checkField('id', 'subscription', 'customer_id', Context::getContext()->customer->id, false);
		
        $this->context->smarty->assign(
            array(
				'iban' => $has_iban,
				'bic' => $has_bic,
				'has_subscription' => $has_subscription
            )
        );
    }

	/**
	* Create a new cart to have a line per product/transaction in relais colis table
	*/
	public function initNewCart()
	{
		$new_cart = new Cart();
		$new_cart->id_currency = $this->context->cookie->id_currency;
		$new_cart->id_lang = $this->context->cookie->id_lang;
		$new_cart->customer_id = $this->context->customer->id;
		$new_cart->id_carrier = (int)Configuration::getGlobalValue('RELAISCOLIS_ID');
		$new_cart->id_address_delivery = $this->context->cart->id_address_delivery;
		$this->context->cart->delete();
		$new_cart->save();
		$this->context->cookie->__set("id_cart", $new_cart->id);
		$this->context->cart = $new_cart;
	}

}