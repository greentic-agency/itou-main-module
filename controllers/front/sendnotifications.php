<?php 

use Modules\Itou\Itou\Module\Service\Sms;
use Modules\Itou\Itou\Module\Service\Email;

/**
 * Itou
 * Send notification SMS or Email
 */
class itousendnotificationsModuleFrontController extends ModuleFrontController
{
    /**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$context = Context::getContext();
        $errors = [];
		if ($context->customer->logged === true) {
            if (Tools::getIsset('sendSms')) {
                try {
                    (new Sms())->sendSms(Tools::getValue('sale_id'));
                } catch (\Exception $e) {
                    if (_PS_MODE_DEV_) {
                        throw $e;
                    }
                    $errors[] = "Une erreur survenue lors de l'envoi du SMS";
                }

                try {
                    $email = new Email();
                    $email->sendEmail(Tools::getValue('sale_id'));
                } catch (\Exception $e) {
                    if (_PS_MODE_DEV_) {
                        throw $e;
                    }
                    $errors[] = "Une erreur survenue lors de l'envoi de l'email";
                }
            }
        }

        if ($errors) {
            $this->context->smarty->assign("errors", $errors);
        }
        
        $link = new Link();
        Tools::redirect($link->getModuleLink('itou', 'sales'));
    }
}