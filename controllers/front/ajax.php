<?php

use Modules\Itou\Itou\Module\Utils;

/**
 * Itou
 * Scripts ajax
 */
class itouajaxModuleFrontController extends ModuleFrontController
{
    /**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$context = Context::getContext();

		if ($context->customer->logged === true) {
            if (Tools::getIsset('searchBuyer') && $login = Tools::getValue('login')) {
                $this->searchBuyer($login);
            }
        }
        $this->setTemplate('module:itou/views/templates/ajax/ajax.tpl');
    }
    
    /**
     * Search if buyer exists to link him to the new product
     */
    public function searchBuyer(string $login)
    {
        $result = false;
        $customer_id = false;

        if(($customer_id = Utils::checkField('id', 'customerextrafields', 'login', $login, true))
            && $customer_id != Context::getContext()->customer->id) {
            $result = true;
        }

        $this->context->smarty->assign(
            array(
                'result' => $result,
                'customer_id' => $customer_id
            )
        );
    }

}