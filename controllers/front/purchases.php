<?php

use Modules\Itou\Itou\Module\Utils;

/**
 * Itou
 * Display Purchases listing for the current customer
 */
class itoupurchasesModuleFrontController extends ModuleFrontController
{
	/**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$p = Tools::getValue('p');
		$n = 5;

		if($this->context->customer->logged === true) {
			$display_link_subscription = true;
			if(Module::isInstalled('productextrafields')) {

				if(Tools::getIsset('addPurchase') && $purchaseRef = Tools::getValue('purchaseReference')) {
					$this->AddPurchase($purchaseRef, $this->context->customer->id);
				}

				$subscription = new \Modules\Itou\Subscription\Subscription($this->context->customer->id);
				if($subscription && $subscription->subscription_id != Configuration::get('SUBSCRIPTION_SIMPLE_ID')) {
					$display_link_subscription = false;
				}

				if($purchases = self::getPurchases($this->context->customer->id, $p, $n)) {

					Utils::paginate(count(self::getPurchases($this->context->customer->id)), $p, $n, 4, 'purchases');

					$this->context->smarty->assign(array(
						'purchases' => $purchases,
						'state' => $this->l('En attente'),
						'display_link_subscription' => $display_link_subscription
					));
				}else{
					$this->errors[]=$this->l('Vous n\'avez pas encore fait d\'achat.');
				}
					
			}else{
				$this->errors[]=$this->l('Vous devez installer le module productextrafields.');
			}

			$this->setTemplate('module:itou/views/templates/front/my-account/listings/purchases-listing.tpl');
		}else{
			Tools::redirect('authentication');
		}
	}

	/**
	 * Link a purchase to a buyer
	 * @params Purchase(product) reference
	 */
	public function AddPurchase($purchaseRef, $buyer_id)
	{
		if($purchase_id = Utils::checkField('id_product', 'product', 'reference', pSQL($purchaseRef), true)) {

			$test = Utils::checkField('buyer_id', 'productextrafields', 'id', $purchase_id, true);

			if($buyer_id != $test && $test != false && $test != '0'){
				$this->errors[]=$this->l('Purchase is already linked to a buyer.');
			}else{
				$productExtraField = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($purchase_id);

				if($productExtraField->seller_id == $buyer_id ){
					$this->errors[]=$this->l('You can\'t add a purchase you\'ve created.');
				}else{
					$productExtraField->buyer_id = $buyer_id;
					$productExtraField->date_send = null;
					$productExtraField->save(true, true);
				}
			}
			
		}else{
			$this->errors[]=$this->l('Purchase not found.');
		}

		$link = new Link();
		$this->redirectWithNotifications($link->getModuleLink('itou','purchases'));
	}

	/**
	 * Get sales list for the current customer
	 * @param int $customer_id 
	 * @param int $id_lang Language id
	 * @param int $start Start number
	 * @param int $limit Number of products to return
	 * @param string $order_by Field for ordering
	 * @param string $order_way Way for ordering (ASC or DESC)
	 * @return array Products details
	 */
	 public static function getPurchases(
		$customer_id,
		$start = '0',
		$limit = '1000',
		$order_by = 'date_add',
		$order_way = 'DESC',
		$only_active = false)
	{

		$context = Context::getContext();
		if ($start < 1) $start = 1;
		$id_lang = $context->language->id;
		$front = true;

		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
			$front = false;	

		$sql = 'SELECT p.*, product_shop.*, pl.*, i.id_image, pef.*
		FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
		LEFT JOIN `'._DB_PREFIX_.'productextrafields` pef ON (p.`id_product` = pef.`id`)
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (p.`id_product` = i.`id_product`)
		WHERE pef.`date_send` IS NOT NULL
			AND pl.`id_lang` = '.(int)$id_lang.'
			AND pef.`buyer_id` = '.(int)$customer_id.
			($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
			($only_active ? ' AND product_shop.`active` = 1' : '').'
		ORDER BY p.`'.pSQL($order_by).'` '.pSQL($order_way).
		' LIMIT '.(((int)$start - 1) * (int)$limit).','.(int)$limit ;
		
		$rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		foreach ($rq as &$row) {
			$row = Product::getTaxesInformations($row);
			if(Module::isInstalled('customerextrafields')) {
				if($row['seller_id'] != 0) {
					$seller = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($row['seller_id']);
					$row['seller'] = $seller;
				}else{
					$row['seller'] = [];
				}
			}
		}

		return ($rq);
	}

}