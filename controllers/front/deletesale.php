<?php

/**
 * @author Lucile Greentic
 * Delete sale action 
 * */
class itoudeletesaleModuleFrontController extends ModuleFrontController
{
    public function initContent()
	{
		parent::initContent();
		$context = Context::getContext();
		if ($context->customer->logged === true) {
            if (Tools::getIsset('deleteProduct')) {
                $id_product = Tools::getValue('sale_id');
                try {
                    $productextrafields = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($id_product);

                    if ($productextrafields->seller_id == $context->customer->id) {
                        $product = new Product((int)$id_product);
                        $product->delete();
                    }
                } catch (\Exception $e) {
                    if (_PS_MODE_DEV_) {
                        throw $e;
                    }
                    $errors[] = "Une erreur est survenue lors de la suppression du produit.";
                }
            }
        }
        if (!empty($errors)) {
            $this->context->smarty->assign("errors", $errors);
        }

        $this->success[] = $this->l('La transaction a bien été supprimée.');

        $link = new Link();
        $this->redirectWithNotifications($link->getModuleLink('itou', 'sales'));
    }
}