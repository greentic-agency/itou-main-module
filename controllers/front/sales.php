<?php
use Modules\Itou\Itou\Module\Utils;

/**
 * Itou
 * Display Sales listing for the current customer
 */
class itousalesModuleFrontController extends ModuleFrontController
{
	const ZONE_FRANCE_ID = 9;
	/**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$context =$this->context;
		$p = Tools::getValue('p');
		$n = 5;

		if ($context->customer->logged === true) {
			if(Module::isInstalled('productextrafields')) {

				$susbcription_customer = Module::isInstalled('subscription') ?
				Utils::checkField('subscription_id', 'subscription', 'customer_id', $context->customer->id, false, true) :
				false; 
				if($sales = self::getSales($this->context->customer->id, $p, $n)) {

					Utils::paginate(count(self::getSales($this->context->customer->id)), $p, $n, 4, 'sales');
					$this->context->smarty->assign(array(
						'susbcription_customer' => $susbcription_customer,
						'sales' => $sales
					));

				}else{
					$this->errors[]=$this->l('Vous n\'avez pas encore créé de vente.');
				}
			}else{
				$this->errors[]=$this->l('Vous devez installer le module productextrafields.');
			}
			$this->setTemplate('module:itou/views/templates/front/my-account/listings/sales-listing.tpl');
		}else{
			Tools::redirect('authentication');
		}
	}

	/**
	 * Get sales list for the current customer
	 * @param int $customer_id 
	 * @param int $id_lang Language id
	 * @param int $start Start number
	 * @param int $limit Number of products to return
	 * @param string $order_by Field for ordering
	 * @param string $order_way Way for ordering (ASC or DESC)
	 * @return array Products details
	 */
	 public static function getSales(
		$customer_id,
		$start = '0',
		$limit = '1000',
		$order_by = 'date_add',
		$order_way = 'DESC',
		$only_active = false)
	{

		$context = Context::getContext();
		if ($start < 1) $start = 1;
		$id_lang = $context->language->id;
		$front = true;

		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
			$front = false;	

		$sql = 'SELECT p.*, product_shop.*, pl.*, i.id_image, pef.*
		FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
		LEFT JOIN `'._DB_PREFIX_.'productextrafields` pef ON (p.`id_product` = pef.`id`)
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (p.`id_product` = i.`id_product`)
		WHERE pl.`id_lang` = '.(int)$id_lang.'
					AND pef.seller_id = '.(int)$customer_id.
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($only_active ? ' AND product_shop.`active` = 1' : '').'
		ORDER BY p.`'.pSQL($order_by).'` '.pSQL($order_way).
		' LIMIT '.(((int)$start - 1) * (int)$limit).','.(int)$limit ;

		$rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		foreach ($rq as &$row) {
			$row = Product::getTaxesInformations($row);

			if($row['order_id'] != '0' && $row['order_id'] != null && $row['is_shipping_cost_paid'] != '0') {
				$order = new Order($row['order_id']);
				$cart = new Cart($order->id_cart);
				$carrier = new Carrier($order->id_carrier);
				$moduleRelaisColis = Module::getInstanceByName('relaiscolis');
				$shipping_cost = $carrier->getDeliveryPriceByWeight($row['weight'], self::ZONE_FRANCE_ID);
				$shipping_cost_final = $moduleRelaisColis->getOrderShippingCost($cart, $shipping_cost, ['customer_id' => $row['buyer_id'], 'cart_weight' => $row['weight']]);
				$row['shipping_cost'] = $shipping_cost_final;
			}

			if(Module::isInstalled('customerextrafields')) {
				if($row['buyer_id'] != 0) {
					$buyer = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($row['buyer_id']);
					$row['buyer'] = $buyer;
				}else{
					$row['buyer'] = [];
				}
			}
		}

		return ($rq);
	}
}