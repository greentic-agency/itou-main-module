<?php

namespace Modules\Itou\Itou;

\spl_autoload_register(function ($class) {
    if (strncmp(__NAMESPACE__, $class, strlen(__NAMESPACE__)) !== 0) return;
    $file = __DIR__ . '/src/' . str_replace('\\', '/', substr($class, strlen(__NAMESPACE__))) . '.php';
    file_exists($file) && require_once $file;
});

require_once  __DIR__ .'/vendor/autoload.php';