<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{itou}prestashop>itou_4b939dc81b428ac878ff79a6c7c4f481'] = 'I-tou';
$_MODULE['<{itou}prestashop>itou_758cc689d19971532ffcd3a9f064e405'] = 'Passerelle de vente/achat de produits pour les particuliers';
$_MODULE['<{itou}prestashop>creationsale_7c8682d92dfb1522b4cc2975608e582d'] = 'Données mises à jour avec succès.';
$_MODULE['<{itou}prestashop>itou_439149be8bfe2e2ff0c156e35bfd5894'] = 'En attente';
$_MODULE['<{itou}prestashop>itou_e6aa8b5be6a8e5a327762fa69d5ed931'] = 'Vous n\'avez pas encore d\'achat.';
$_MODULE['<{itou}prestashop>itou_0a89f783570a685a50fe28ce30db1a2a'] = 'Vous devez installer le module \"productextrafields\".';
$_MODULE['<{itou}prestashop>itou_2ba4f4b8d925f239e015b5a4b43d4497'] = 'L\'achat est déjà affilié à un autre acheteur.';
$_MODULE['<{itou}prestashop>itou_dadab2300e13433cd6b1e5724cf01fc3'] = 'Vous ne pouvez pas ajouter un achat que vous avez créée.';
$_MODULE['<{itou}prestashop>itou_c5db9f8fc4588208b91147f740f4b75d'] = 'Achat non trouvé.';
$_MODULE['<{itou}prestashop>itou_439149be8bfe2e2ff0c156e35bfd5894'] = 'En attente';
$_MODULE['<{itou}prestashop>itou_796e206c5243208162f9ed4341145b03'] = 'Vous n\'avez pas encore effectué de vente.';
$_MODULE['<{itou}prestashop>configurable_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{itou}prestashop>configurable_630f6dc397fe74e52d5189e2c80f282b'] = 'Retour à la liste';
