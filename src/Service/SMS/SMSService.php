<?php

namespace Modules\Itou\Itou\Service\SMS;

use Modules\Itou\Itou\Service\SMS\Adapter\Interfaces\AdapterInterface;
use Modules\Itou\Itou\Module\Utils;

class SMSService
{
    private $adapter;
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
    public function getAdapter(): AdapterInterface
    {
        return $this->adapter;
    }
    public function send(string $from, string $to, string $message): bool
    {
        return $this->adapter->send($from, Utils::formattedPhoneSms($to), $message);
    }
}
