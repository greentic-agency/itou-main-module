<?php

namespace Modules\Itou\Itou\Service\SMS\Adapter\Exception;

use Modules\Itou\Itou\Service\SMS\Adapter\Exception\AdapterException;

class RejectedAdapterException extends AdapterException {}