<?php

namespace Modules\Itou\Itou\Service\SMS\Adapter;

use Nexmo\Client\Credentials\Basic;
use Nexmo\Client;
use Modules\Itou\Itou\Service\SMS\Adapter\Interfaces\AdapterInterface;
use Modules\Itou\Itou\Service\SMS\Adapter\Exception\RejectedAdapterException;

class NexmoAdapter implements AdapterInterface
{
    private $key;
    private $secret;
    public function __construct(?string $key = null, ?string $secret = null)
    {
        if ($key !== null) {
            $this->setKey($key);
        }
        if ($secret !== null) {
            $this->setSecret($secret);
        }
    }
    public function setKey(string $key): NexmoAdapter
    {
        $this->key = $key;
        return $this;
    }
    public function getKey(): string
    {
        return $this->key;
    }
    public function setSecret(string $secret): NexmoAdapter
    {
        $this->secret = $secret;
        return $this;
    }
    public function getSecret(): string
    {
        return $this->secret;
    }
    /**
     * @throws RejectedAdapterException
     */
    public function send(string $from, string $to, string $message): bool
    {
        $service = new Client(new Basic($this->getKey(), $this->getSecret()));
        
        if ($message = $service->message()->send(['to' => $to, 'from' => $from, 'text' => $message])) {
            if (200 !== ($code = $message->getResponse()->getStatusCode())) {
                throw new RejectedAdapterException($message->getResponse()->getReasonPhrase(), $code);
            }
        }

        return true;
    }
}