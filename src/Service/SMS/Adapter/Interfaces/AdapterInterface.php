<?php

namespace Modules\Itou\Itou\Service\SMS\Adapter\Interfaces;

interface AdapterInterface
{
    public function send(string $from, string $to, string $message): bool;
}