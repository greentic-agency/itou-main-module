<?php

namespace Modules\Itou\Itou;

use Modules\Itou\Itou\Module\Configurable;
use Modules\Itou\Itou\Module\Installable;
use Modules\Itou\Itou\Module\Utils;
use Modules\Itou\Itou\Module\Upload;
use Modules\Itou\Itou\Module\Service\Sms;
use Modules\Itou\Itou\Module\Service\Email;
use Modules\Itou\Itou\Module\Service\Label;
use Module as PrestashopModule;
use Order;
use Db;
use Context;
use Configuration;

class Module extends PrestashopModule
{
    use Configurable;
    use Installable;
    use Utils;
    use Upload;

    protected $hooks = [
        'displayCustomerAccount',
        'actionValidateOrder',
        'actionCartSave',
        'moduleRoutes',
        'beforeAdminOrdersControllerRenderList',
        'displayAdminOrderContentOrder',
        'productExtraFieldsModelSendNotificationCancelling',
        'actionOrderStatusPostUpdate'
    ];

    protected $metas = [
        'sales' => 'Mes ventes',
        'purchases' => 'Mes achats',
        'creationsale' => 'Créer une transaction'
    ];

    public function hookActionOrderStatusPostUpdate($params)
    {
        if ($params['newOrderStatus']->id == Configuration::get('STATE_ORDER_CANCELED')) {

            $productsToExclude =  array_values(Configuration::getMultiple(['SUBSCRIPTION_SIMPLE_ID',
            'SUBSCRIPTION_YEARLY_ID',
            'SUBSCRIPTION_MONTHLY_ID']));

            $bankcharges = Configuration::get('BANK_CHARGES_ID');

            $order = new Order($params['id_order']);
            $products = $order->getProducts();

            foreach($products as $product) {
                if(!in_array($product['product_id'], $productsToExclude) && $bankcharges != $product['product_id']) {

                    $productextrafields = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($product['product_id']);
                    $productextrafields->is_cancelled = true;
                    $productextrafields->update();

                    $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                    $sms->sendSms($product['product_id'], 'cancelOrder');
                    /** SEND EMAIL TO BUYER AND SELLER */
                    $email = new \Modules\Itou\Itou\Module\Service\Email();
                    $email->sendEmail($product['product_id'], 'cancelOrderBuyer');
                    $email->sendEmail($product['product_id'], 'cancelOrderSeller');
        
                    /** SEND TO ADMIN */
                    $sms->sendSms($product['product_id'], 'noticeAdminCancel');
                    $email->sendEmail($product['product_id'], 'noticeAdminCancel');
                } elseif(in_array($product['product_id'], $productsToExclude)) {

                    /** If subscription cancelled, juste put to basic subscription le field subscription_id */
                    Db::getInstance()->execute("UPDATE `ps_subscription` SET `subscription_id` = ".Configuration::get('SUBSCRIPTION_SIMPLE_ID').",`date_end` = '2060-01-01 00:00:00' WHERE `customer_id` = ".pSQL($order->id_customer).";");
                }
            }
        }
    }

    /**
     * Send notification when cancelling a sale / product
     * when buyer didn't pay yet
     * Called in module Productextrafields in function SendNotificationCancelling
     * @param array $params
     */
    public function hookProductExtraFieldsModelSendNotificationCancelling($params)
    {
        $productExtraFields = $params['productExtraFields'];
        if (empty($productExtraFields->order_id)) {
            /** If order not yet created we just send SMS to cancel the transaction / product */
            /** SEND SMS TO BUYER AND SELLER */
            $sms = new \Modules\Itou\Itou\Module\Service\Sms();
            $sms->sendSms($productExtraFields->id, 'cancelProduct');
            /** SEND EMAIL TO BUYER AND SELLER */
            $email = new \Modules\Itou\Itou\Module\Service\Email();
            $email->sendEmail($productExtraFields->id, 'cancelProductBuyer');
            $email->sendEmail($productExtraFields->id, 'cancelProductSeller');

            $sms->sendSms($productExtraFields->id, 'noticeAdminCancel');
            $email->sendEmail($productExtraFields->id, 'noticeAdminCancel');

        } else { 
            /** Otherwise we send SMS/ email special to cancellation order */
            $sms = new \Modules\Itou\Itou\Module\Service\Sms();
            $sms->sendSms($productExtraFields->id, 'cancelOrder');
            /** SEND EMAIL TO BUYER AND SELLER */
            $email = new \Modules\Itou\Itou\Module\Service\Email();
            $email->sendEmail($productExtraFields->id, 'cancelOrderBuyer');
            $email->sendEmail($productExtraFields->id, 'cancelOrderSeller');

            $sms->sendSms($productExtraFields->id, 'noticeAdminCancel');
            $email->sendEmail($productExtraFields->id, 'noticeAdminCancel');
        }
    }

    /**
     * Hook displays the new links to account menu
     */
    public function hookDisplayCustomerAccount(){
        return $this->display(_PS_MODULE_DIR_.'itou/itou.php', 'views/templates/front/my-account/menu/links.tpl'); 
    }

     /**
     * Update order_id in productextrafields table
     * Send SMS if seller wants to receive notifications by SMS
     * @param array $params
     */
    public function hookActionValidateOrder(array $params)
    {
        $context = Context::getContext();
        $products_order = $params['order']->getProducts();
        $order_id = $params['order']->id;
        $susbcription_ids = Configuration::get('SUBSCRIPTION_IDS');

        foreach($products_order as $product) {
            if(!in_array($product['product_id'], json_decode($susbcription_ids))) {
                $productExtraFields = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($product['product_id']);
                $productExtraFields->order_id = $order_id;
            
                if(is_null($productExtraFields->date_send) || $productExtraFields->date_send == '0000-00-00 00:00:00'){
                    $productExtraFields->date_send = null;
                }
             
                $productExtraFields->save(true, true);
                
                /**  Send SMS if seller wants to receive notifications by SMS */
                $customerExtraFields = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($productExtraFields->seller_id);
            
                if($customerExtraFields->is_noticed_by_text_message == true) {
                    $sms = new Sms();
                    $sms->sendSms($product['product_id'], 'payment');
                }
               
                $email = new Email();
                $email->sendEmail($product['product_id'], 'payment');

                /** Send label to the seller if it's order with no subscription */
                $label = new Label();
                $label->processLabel($order_id);
            }
            break;
        }
    }
}