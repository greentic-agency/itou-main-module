<?php
namespace Modules\Itou\Itou\Module;

use Meta;
use Language;

trait Installable
{
/**
     * Module installation
     * @return BOOL
     */
    public function install() 
    {
        if($success = parent::install()) {
            foreach ($this->hooks as $hook) {
                $success = $success && $this->registerHook($hook);
            }
            foreach ($this->metas as $slug => $title) {
                $page = sprintf('module-%s-%s', $this->name, $slug);
                if (Meta::getMetaByPage($page, $this->context->language->id)) {
                    continue;
                }
                $meta = new Meta();
                $meta->page = $page;
                $meta->configurable = 1;
                foreach (Language::getLanguages() as $language) {
                    $meta->title[$language['id_lang']] = $this->l($title);
                    $meta->url_rewrite[$language['id_lang']] = $page;
                }
                $meta->add();
            }
        }
        return $success;
    }

    /**
     * Module uninstallation
     * @return BOOL
     */
    public function uninstall() 
    {
        if($success = parent::uninstall()) {
            foreach ($this->hooks as $hook) {
                $success = $success && $this->unregisterHook($hook);
            }
            foreach (Meta::getMetas() as $slug => $title) {
                if (false !== strpos($meta['page'], sprintf('module-%s-', $this->name))) {
                    $meta = new Meta((int) $meta['id_meta']);
                    $meta->delete();
                }
            }
        }
            return $success;
    }

}