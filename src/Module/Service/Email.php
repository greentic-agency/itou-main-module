<?php

namespace Modules\Itou\Itou\Module\Service;

use Product;
use Customer;
use Context;
use Configuration;
use Mail;
use Order;
use Modules\Itou\Itou\Module\Service\Message;

class Email
{
    /**
     * Send Email to the seller / buyer
     * @param int $sale_id from productextrafields
     */
    public function sendEmail($sale_id, $action = 'sale')
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;
        $sale = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($sale_id);
        $seller = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($sale->seller_id);
        $buyer = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($sale->buyer_id);
        $product = new Product($sale_id, false, $id_lang);
        $sale->name = $product->name;
        $sale->reference = $product->reference;
       
        $sale->price = number_format($product->price,2);
        $sale->orderTotal = null;
        
        if(!empty($sale->order_id)) {
            $order = new Order($sale->order_id);
            $sale->orderTotal =  number_format($order->getOrdersTotalPaid(),2);
        }

        $buyerModel = new Customer($sale->buyer_id);
        $sellerModel = new Customer($sale->seller_id);
    
        switch($action) {
            case 'sale': 
                $message = Message::messageSale($buyer, $seller, $sale);
                $sujet = 'Demande d\'achat pour ' . $sale->name;
                $template = 'sale';
                $to = $buyerModel->email;
                $sale->date_send = date("Y-m-d H:i:s");
                $sale->update();
            break;
            case 'payment':
                $message = Message::messagePayment($buyer, $seller, $sale);
                $sujet = 'Vente effectuée pour ' . $sale->name;
                $template = 'paymentlink';
                $to = $sellerModel->email;
            break;
            case 'relaunch':
                $message = Message::messageRelaunch($buyer, $seller, $sale);
                $sujet = 'Relance de paiement pour ' . $sale->name;
                $template = 'relaunch';
                $to = $buyerModel->email;
            break;
            case 'cancellationAfterRelaunchBuyer':
                $message = Message::messageCancellationAfterRelaunchBuyer($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellationAfterRelaunchBuyer';
                $to = $buyerModel->email;
            break;
            case 'cancellationAfterRelaunchSeller':
                $message = Message::messageCancellationAfterRelaunchSeller($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellationAfterRelaunchSeller';
                $to = $sellerModel->email;
            break;
            case 'relaunchSellerToSendPackage':
                $message = Message::messageRelaunchSellerToSendPackage($buyer, $seller, $sale);
                $sujet = 'Relance d\'envoi colis pour ' . $sale->name;
                $template = 'relaunchSellerToSendPackage';
                $to = $sellerModel->email;
            break;
            case 'cancellationAfterRelaunchSellerToSendPackageBuyer':
                $message = Message::messageCancellationAfterRelaunchSellerToSendPackageBuyer($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellationAfterRelaunchSellerToSendPackageBuyer';
                $to = $buyerModel->email;
            break;
            case 'cancellationAfterRelaunchSellerToSendPackageSeller':
                $message = Message::messageCancellationAfterRelaunchSellerToSendPackageSeller($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellationAfterRelaunchSellerToSendPackageSeller';
                $to = $sellerModel->email;
            break;
            case 'packageInRelais':
                $message = Message::messagePackageInRelais($buyer, $seller, $sale);
                $sujet = 'N\'oubliez pas d\'aller chercher votre colis ' . $sale->name;
                $template = 'packageInRelais';
                $to = $buyerModel->email;
            break;
            case 'cancellingPackageInRelaisBuyer':
                $message = Message::messageCancellingPackageInRelaisBuyer($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellingPackageInRelaisBuyer';
                $to = $buyerModel->email;
            break;
            case 'cancellingPackageInRelaisSeller':
                $message = Message::messageCancellingPackageInRelaisSeller($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancellingPackageInRelaisSeller';
                $to = $sellerModel->email;
            break;
            case 'paySeller':
                $message = Message::messagePaySeller($buyer, $seller, $sale);
                $sujet = 'Vous allez bientôt être crédité pour votre commande ' . $sale->name;
                $template = 'paySeller';
                $to = $sellerModel->email;
            break;
            case 'debitBuyer':
                $message = Message::messageDebitBuyer($buyer, $seller, $sale);
                $sujet = 'Vous allez bientôt être débité pour votre commande ' . $sale->name;
                $template = 'paySeller';
                $to = $buyerModel->email;
            break;
            case 'cancelProductBuyer':
                $message = Message::messageCancelProductBuyer($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancelProductBuyer';
                $to = $buyerModel->email;
            break;
            case 'cancelProductSeller':
                $message = Message::messageCancelProductSeller($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancelProductSeller';
                $to = $sellerModel->email;
            break;
            case 'cancelOrderBuyer':
                $message = Message::messageCancelOrderBuyer($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancelOrderBuyer';
                $to = $buyerModel->email;
            break;
            case 'cancelOrderSeller':
                $message = Message::messageCancelOrderSeller($buyer, $seller, $sale);
                $sujet = 'Annulation de commande pour ' . $sale->name;
                $template = 'cancelOrderSeller';
                $to = $sellerModel->email;
            break;
            case 'noticeAdminCancel':
                $message = Message::messageNoticeAdmin($sale);
                $sujet = 'Commande annulée à rembourser';
                $template = 'messageNoticeAdmin';
                $to = Configuration::get('PS_SHOP_EMAIL');
            break;
        }

        $vars = array(
            '{shop_url}' => Configuration::get('PS_SHOP_DOMAIN_SSL'),
            '{shop_name}' => Configuration::get('PS_SHOP_NAME', null, null, 1),
            '{message}' => $message[0]
        );
        
        if(!Mail::send(
            (int)$id_lang,
            $template,
            Mail::l($sujet, (int)$id_lang),
            $vars,
            $to,
            null,
            null,
            null,
            null,
            null,
            _PS_MODULE_DIR_.'itou/mails/'
        )){
           // throw new \Exception($to." Email not sent \itou\src\Module\Service\Email.php");
           PrestaShopLogger::addLog($to." Email not sent \itou\src\Module\Service\Email.php",4);
    
        }
    }
               
    public function sendLabel($filename, $seller, $product_sold)
	{
		if(!$pdf = file_get_contents($filename)) {
			throw new \Exception('File '.$filename.' doesn\'t exist.');
		}

		$attach = [
			'content' => $pdf,
			'name' => 'Etiquette.pdf',
			'mime' => 'application/pdf'
        ];
        
        $message = "Bonjour, Vous trouverez en pièce jointe votre étiquette à imprimer afin d'envoyer votre colis ". $product_sold->reference . ". Vous disposez de 5 jours ouvrés pour déposer votre colis. Merci et à bientôt sur www.i-tou.fr !";

        $vars = array(
            '{shop_url}' => Configuration::get('PS_SHOP_DOMAIN_SSL'),
            '{shop_name}' => Configuration::get('PS_SHOP_NAME', null, null, 1),
            '{message}' => $message
        );

		if(!Mail::Send(
			1,
			'sendLabel',
			'Votre étiquette pour la vente ' .$product_sold->name,
			$vars,
			$seller->email,
			$seller->firstname . ' ' . $seller->lastname,
			null,
			null,
			$attach,
			null,
			_PS_MODULE_DIR_.'itou/mails/'
		)){
			throw new \Exception("Problem during email with label sent - function sendLabel()");
        }
        
        unlink($filename);
	}
}