<?php

namespace Modules\Itou\Itou\Module\Service;

use Product;
use Configuration;
use Order;
use Modules\Itou\Itou\Service\SMS\SMSService;
use Modules\Itou\Itou\Module\Service\Message;
use Modules\Itou\Itou\Service\SMS\Adapter\NexmoAdapter;
use Modules\Itou\Itou\Service\SMS\Adapter\Exception\RejectedAdapterException;

class Sms
{
    const PREFIX = 'i-tou:';
    const FROM = 'itou';
    /**
     * Send SMS to the seller with the payment link
     * @param int $sale_id 
     * @param string $to (buyer or seller)
     * @param string $action sale/payment/traking 
     */
    public function sendSms($sale_id, $action = 'sale')
    { 
        $sale = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($sale_id);
        $seller = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($sale->seller_id);
        $buyer = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($sale->buyer_id);
        $product = new Product($sale_id, false, Configuration::get('PS_LANG_DEFAULT'));
        $sale->name = $product->name;
        $sale->reference = $product->reference;
        $sale->price = number_format($product->price,2);
        $sale->orderTotal = null;

        if(!empty($sale->order_id)) {
            $order = new Order($sale->order_id);
            $sale->orderTotal =  number_format($order->getOrdersTotalPaid(),2);
        }

        /** A SUPPRIMER QUAND LE SERVICE SMS EST OK */ //return true;
       
        //$service = new SMSService(new NexmoAdapter('6dc05ae7', 'o6mtUK2casY5SDK6'));
        $service = new SMSService(new NexmoAdapter('3ddb2830', '9IBoDgs7DhupqPzp'));

        switch($action) {
            case 'sale': 
                $message_buyer = Message::messageSale($buyer, $seller, $sale);
                $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                $sale->date_send = date("Y-m-d H:i:s");
                $sale->update();
            break;
            case 'payment':
                $message_seller = Message::messagePayment($buyer, $seller, $sale);
               // $message_buyer = Message::messageSale($buyer, $seller, $sale);
                $service->send(self::FROM, $seller->phone, $message_seller[1]);
               // $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
            break;
            case 'relaunch':
                if($buyer->is_noticed_by_text_message == true) {
                    $message_buyer = Message::messageRelaunch($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                }
            break;
            case 'cancellationAfterRelaunch':
                if($buyer->is_noticed_by_text_message == true) {
                    $message_buyer = Message::messageCancellationAfterRelaunchBuyer($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                }
                if($seller->is_noticed_by_text_message == true) {
                    $message_seller = Message::messageCancellationAfterRelaunchSeller($buyer, $seller, $sale);
                    $service->send(self::FROM, $seller->phone, $message_seller[1]);
                }
            break;
            case 'relaunchSellerToSendPackage':
                if($seller->is_noticed_by_text_message == true) {
                    $message_seller = Message::messageRelaunchSellerToSendPackage($buyer, $seller, $sale);
                    $service->send(self::FROM, $seller->phone, $message_seller[1]);
                }
            break;
            case 'cancellationAfterRelaunchSellerToSendPackage':
                if($seller->is_noticed_by_text_message == true) {
                    $message_seller = Message::messageCancellationAfterRelaunchSellerToSendPackageSeller($buyer, $seller, $sale);
                    $service->send(self::FROM, $seller->phone, $message_seller[1]);
                }
                if($buyer->is_noticed_by_text_message == true) {
                    $message_buyer = Message::messageCancellationAfterRelaunchSellerToSendPackageBuyer($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                }
            break;
            case 'packageInRelais':
                if($buyer->is_noticed_by_text_message == true) {
                    $message_buyer = Message::messagePackageInRelais($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                }
            break;
            case 'cancellingAfterpackageInRelais':
                if($buyer->is_noticed_by_text_message == true) {
                    $message_buyer = Message::messageCancellingPackageInRelaisBuyer($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_buyer[1]);
                }
                if($seller->is_noticed_by_text_message == true) {
                    $message_seller = Message::messageCancellingPackageInRelaisSeller($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message_seller[1]);
                }
            break;
            case 'paySeller':
               /* if($seller->is_noticed_by_text_message == true) {
                    $message_seller = self::PREFIX . Message::messagePaySeller($buyer, $seller, $sale);
                    $service->message()->send(['to' => $seller->phone, 'from' => self::PREFIX, 'text' => $message_seller]);
                }*/
            break;
            case 'debitBuyer':
               /* if($buyer->is_noticed_by_text_message == true) {
                    $message = self::PREFIX . Message::messageDebitBuyer($buyer, $seller, $sale);
                    $service->message()->send(['to' => $buyer->phone, 'from' => self::PREFIX, 'text' => $message]);
                }*/
            break;
            case 'cancelProduct':
                if($buyer->is_noticed_by_text_message == true) {
                    $message = Message::messageCancelProductBuyer($buyer, $seller, $sale);
                    $service->send(self::FROM, $buyer->phone, $message[1]);
                }
                if($seller->is_noticed_by_text_message == true) {
                    $message = Message::messageCancelProductSeller($buyer, $seller, $sale);
                    $service->send(self::FROM, $seller->phone, $message[1]);
                }
            break;
            case 'cancelOrder':
            if($buyer->is_noticed_by_text_message == true) {
                $message = Message::messageCancelOrderBuyer($buyer, $seller, $sale);
                $service->send(self::FROM, $buyer->phone, $message[1]);
            }
            if($seller->is_noticed_by_text_message == true) {
                $message = Message::messageCancelOrderSeller($buyer, $seller, $sale);
                $service->send(self::FROM, $seller->phone, $message[1]);
            }
            break;
            case 'noticeAdminCancel': //commenter oon envoie seulement par mail.
                //$message = Message::messageNoticeAdmin($sale);
                //$service->send(self::FROM, Configuration::get('SMS_NOTIFICATION_ADMINMOBILE'), $message[1]);
            break;
        }
    }

    
}