<?php
namespace Modules\Itou\Itou\Module\Service;

require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisOrder.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisOrderPdf.php';

use Product;
use Customer;
use Module;
use Configuration;
use Order;
use Exception;
use Cart;
use DbQuery;
use Db;
use Modules\Itou\Itou\Module\Service\Email;
use Modules\Itou\Itou\Module\Utils;
use PrestaShopLogger;
use RelaisColisOrder;
use RelaisColisOrderPdf;


class Label
{
    const LOGIN_RELAISCOLIS = 'WSCTOCT7';
    const PASSWORD_RELAISCOLIS = 'ITOU2018';
    const CODE_ENSEIGNE_RELAISCOLIS = 'T7';
    const TYPE_CLIENT = 'perso';

    /**
     * @param $order_id
     */
    public function processLabel($order_id)
    {
        $order = new Order($order_id);
		$cart = new Cart($order->id_cart);
		$product_sold_id = $this->getProductOrder($order->id);

		if(is_null($product_sold_id))
			return false;
		
			$productextrafields_sold = new \Modules\Itou\ProductExtraFields\ProductExtraFieldsModel($product_sold_id);
			$product_sold = new Product($product_sold_id, false, $id_lang = 1);
			$seller = $this->getCustomerInfo($order, $productextrafields_sold, 'seller');
			$buyer = $this->getCustomerInfo($order, $productextrafields_sold, 'buyer');

			$params = [
				"{UserName}" => self::LOGIN_RELAISCOLIS,
				"{Password}" => self::PASSWORD_RELAISCOLIS,
				"{NumEnvoi}" => $order->id,
				"{NomExpediteur}" => $seller->firstname . ' ' . $seller->lastname,
				"{RelaisDepot}" => $seller->pointRelais['rel'],
				"{ExpediteurTelDom}" => "",
				"{ExpediteurTelPor}" => Utils::formattedPhone($seller->customerExtraFields->phone),
				"{ExpediteurEmail}" =>  $seller->email,
				"{ExpediteurAdresse1}" => $seller->customerExtraFields->city,
				"{ExpediteurAdresse2}" => "",
				"{ExpediteurCodePostal}" => $seller->customerExtraFields->zip_code,
				"{ExpediteurVille}" => $seller->customerExtraFields->city,
				"{CodeEnseigne}" => self::CODE_ENSEIGNE_RELAISCOLIS,
				"{NomClient}" => $buyer->firstname . ' ' . $buyer->lastname,
				"{NumClient}" => $buyer->id,
				"{Adresse1}" => $buyer->customerExtraFields->city,
				"{Adresse2}" => "",
				"{CodePostal}" => $buyer->customerExtraFields->zip_code,
				"{Ville}" => $buyer->customerExtraFields->city,
				"{ClientTelDom}" => "",
				"{ClientTelPor}" => Utils::formattedPhone($buyer->customerExtraFields->phone),
				"{ClientEmail}" => $buyer->email,
				"{RelaisDestination}" => $buyer->pointRelais['rel'],
				"{Poids}" => ($cart->getTotalWeight()-0.001)*1000, // weight of bank charges 0.001
				"{NbPaquet}" => "1",
				"{TypeClient}" => self::TYPE_CLIENT,
				"{IdentifiantCompte}" => "",
				"{OptionSmart}" => "0",
				"{NumCommande}" => $order->id
            ];
            
			$filename = tempnam('', '');
	
			try {
				$this->generateLabel($params, $filename, $order->id);

				$email = new Email();
				$email->sendLabel($filename, $seller, $product_sold);
				// si on le laisse ici, le status est maj avant que le statut paiement accepté soit ok.
				// moved in override PaymentModule.php Validate Order
				//Utils::changeOrderState($order_id, Configuration::get('ITOU_STATE_LABEL_SENT'));

			} catch(Exception $e) {
				die($e->getMessage());
			}
    }

    /**
     * Get the all the infos needed about the customer depending of
     * the order, the product sold and the customer's type
     * @param object $order
     * @param object $product
     * @param string $type
     * @return object
     */
    public function getCustomerInfo(object $order, object $product, $type = "seller")
    {
        if($type == 'seller') {
            $customer = new Customer($product->seller_id);
            $customer->pointRelais = $this->findRelaisInfos($product->seller_relais_id, 'seller');
        } else {
            $customer = new Customer($order->id_customer);
            $customer->pointRelais = $this->findRelaisInfos($order->id_cart, 'buyer');
        }

        $customer->customerExtraFields = new \Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel($customer->id);
      
		return $customer;
	}
	
	/**
	 * @param int $seller_relais_id
	 */
	public function findRelaisInfos($relais_id, $type = 'seller')
    {
        if(Module::isInstalled('relaiscolis') && $relais_id != '') {

			if($type == 'seller') {
				$query = (new DbQuery())
						->select('*')
						->from('relaiscolis_info', 'r')
						->where('r.id_relais_colis_info = ' . pSQL($relais_id));
			} else {
				$query = (new DbQuery())
					->select('*')
					->from('relaiscolis_info', 'r')
					->where('r.id_cart = ' . pSQL($relais_id));
			}
            
        if(Db::getInstance()->getRow($query))
            return Db::getInstance()->getRow($query);
        else
            return [];
        }

        return [];
    }
    
	/**
     * Get the product created by the seller
     * for the given order ID to get all the data we wanted
     * For the buyer / seller etc
     * We excluded products bankcharges and subscriptions
     * @param int id_order
     * @return int id_product
     */
    public function getProductOrder($id_order)
    {
        $order = new Order($id_order);
        $products = $order->getProducts();
        $productsToExclude =  array_values(Configuration::getMultiple(['SUBSCRIPTION_SIMPLE_ID',
                                                        'BANK_CHARGES_ID',
                                                        'SUBSCRIPTION_YEARLY_ID',
                                                        'SUBSCRIPTION_MONTHLY_ID']));
        if(empty($productsToExclude)) {
            throw new \Exception("No subscription id or bankcharges id found on table ps_configuration. \itou\src\Module\Service\cronstateorder.php function getProductsOrder(id_order)");
        }

        $id_product = null;

        foreach($products as $product) {
            if(!in_array($product['product_id'], $productsToExclude)) {
                $id_product = $product['product_id'];
            }
        }
        return $id_product;
	}
    
    /**
     * @param array $params
     * @param string $filename
     */
	public function generateLabel($params, $filename = 'label.pdf', $id_order)
	{
		$payload = file_get_contents(dirname(__FILE__)."/../../../request.soap");
		$payload = strtr($payload, $params);
		$context = stream_context_create([
			"http" => [
				"method" => "POST",
				"header" => implode("\r\n", [
					'Content-Type: text/xml; charset="utf-8"',
					"Accept: text/xml",
					"Cache-Control: no-cache",
					"Pragma: no-cache",
					"SOAPAction: http://www.sogep.com/EnregistrementCToC",
					"Content-Length: " . strlen($payload)
		
				]),
				"content" => $payload
			]
		]);
		
		$response = file_get_contents(
			"http://service.relaiscolis.com/WSEnregistrementCtoC/WSEnregistrementCtoC.asmx?wsdl",
			false,
			$context
		);

		$xml = simplexml_load_string($response);
		$xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
		$body = $xml->xpath('//soap:Body');
		$body = $body[0]->EnregistrementCToCResponse->EnregistrementCToCResult->ListCToCResponse->CToCResponse;

		//if (_PS_MODE_DEV_) {
			if($body->ResponseStatus == 'Error') {
				PrestaShopLogger::addLog('Module Itou/Service/ Label::generateLabel() is not generated - ' . $body->Errors->ProcessingRequestError->ErrorDescription, 4);
			}
		//}

     	$pdf_number = $body->CToCInfos->NumExpedition;
		$this->updatePdfNumberRelaisColis($pdf_number, $id_order);

		$cab = $body->CToCInfos->CabEtiquetteColis;
		
		file_put_contents($filename, base64_decode((string) $cab));

	}

	/**
	 * Update field pdf_number mandatory to update order status by relaiscolis
	 * @param string $pdf_number
     * @param int $id_order
	*/
	public function updatePdfNumberRelaisColis($pdf_number, $id_order)
	{
		if($pdf_number != "") {
			$pdf_number .= "01";
			$id_relais_colis_order = RelaisColisOrder::getRelaisColisOrderId((int)$id_order);
			$relais_colis_pdf = new RelaisColisOrderPdf();
			$relais_colis_pdf->pdf_number = (string)$pdf_number;
			$relais_colis_pdf->package_number = (string)'0';
			$relais_colis_pdf->id_relais_colis_order = $id_relais_colis_order;
			$relais_colis_pdf->save();

			Db::getInstance()->execute('UPDATE `ps_relaiscolis_order` SET `pdf_number` = "'.$pdf_number.'" WHERE `id_order` = '.pSQL($id_order).';');
		}
	}
}