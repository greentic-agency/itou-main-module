<?php

namespace Modules\Itou\Itou\Module\Service;
use Link;

class Message
{
     /**
     * Message sent by the seller to the buyer
     * @param buyer entity
     * @param seller entity
     * @param sale entity
     */
    public static function messageSale($buyer, $seller, $sale)
    {
        $link = new Link();
        $addTocart = $link->getAddToCartURL($sale->id, null);
        $message[0] = 'Vous souhaitez acheter l\'article «'.$sale->name.'»! Le vendeur '.$seller->login.' aimerait vous adresser ce colis via www.i-tou.fr sous la référence '.$sale->reference.'. RDV sur ce site dans la rubrique « mon compte / vos achats » pour régler et valider votre transaction d\'un montant de '.$sale->price.'€. Vous disposez de 24h pour profiter de cet article. Passé ce délai, la commande sera annulée. A très vite sur www.i-tou.fr !';
        $message[1] = "Payez votre article réf ".$sale->reference." sous 24h sur www.i-tou.fr";
        return $message;
    }

    /**
     * Message sent to the seller after payment by the buyer
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messagePayment($buyer, $seller, $sale)
    {
        $message[0] = "Félicitations, vous avez vendu votre article «".$sale->name."» ! L'acheteur ".$buyer->login." a réglé le montant de ".$sale->price." € sous la référence ".$sale->reference." sur le site www.i-tou.fr.  Vous disposez de 5 jours ouvrés pour télécharger et imprimer votre bordereau d'envoi reçu par e-mail et déposer votre article dans le Relais Colis choisi lors de la création de l'article. Passé ce délai, la commande sera annulée. A très vite sur www.i-tou.fr!";
        $message[1] = "La réf ".$sale->reference." a été payée. Etiquette à imprimer sur votre email/i-tou.fr";
        return $message;
    }

    /**
     * Message sent to the buyer when he didn't pay yet after 24h
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageRelaunch($buyer, $seller, $sale)
    {
        $message[0] = 'Pensez à régler votre achat «'.$sale->name.'», référence '.$sale->reference.', pour que '.$seller->login.' puisse expédier votre colis via www.i-tou.fr ! Si la somme de '.$sale->price.' € n\'est pas réglée sous 24h, votre commande sera alors annulée. A très vite sur notre site www.i-tou.fr !';
        $message[1] = "Il reste 24h pour régler votre achat réf ".$sale->reference." sinon elle sera annulée/i-tou.fr";
        return $message;
    }
    
    /**
     * Message sent to the buyer when he didn't pay yet after 48h
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellationAfterRelaunchBuyer($buyer, $seller, $sale)
    {
        $message[0] ='Nous vous informons que, malgré notre relance, votre achat de l\'article «'.$sale->name.'» n\'a pas été réglé à temps. La commande portant la référence '.$sale->reference.' est donc annulée. Nous en avons déjà informé '.$seller->login.'. A très vite sur notre site www.i-tou.fr !';
        $message[1] = "Votre achat ".$sale->reference." n'a pas été réglé à temps et est annulé Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message sent to the seller when the buyer didn't pay yet after 48h
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellationAfterRelaunchSeller($buyer, $seller, $sale)
    {
        $message[0] = 'Nous vous informons que, malgré notre relance envoyée à '.$buyer->login.', la somme pour l\'achat de «'.$sale->name.'», référence '.$sale->reference.', n\'a pas été réglée sur notre site www.i-tou.fr. Nous sommes donc désolés de devoir annuler cette commande. A très vite sur notre site www.i-tou.fr !';
        $message[1] = "Votre vente ".$sale->reference." n'a pas été réglée par l'acheteur et est annulée. Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message sent to the seller when he didn't send the package yet after 72h
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageRelaunchSellerToSendPackage($buyer, $seller, $sale)
    {
        $message[0] = 'Vous avez vendu votre article « '.$sale->name.' » à l\'acheteur '.$buyer->login.' d\'un montant de '.$sale->price.' €. Nous nous apercevons que celui-ci n\'est toujours pas expédié, il vous reste 48h pour vous rendre dans votre relais colis, passé ce délai la commande référence '.$sale->reference.' sera annulée. Une expédition rapide = un payement rapide aussi !!!! A très vite, sur notre site www.i-tou.fr. L\'équipe i-tou';
        $message[1] = "Il vous reste 48h pour expédier votre colis ".$sale->reference.". Plus d'info sur i-tou.fr";
        return $message;
    }
    
    /**
     * Message cancellation
     * sent to the seller when he didn't send the package after 6 days
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellationAfterRelaunchSellerToSendPackageSeller($buyer, $seller, $sale)
    {
        $message[0] = 'Vous aviez vendu l\'article « '.$sale->name.' ». Il n\'a pas été expédié à temps au relais colis de départ. Nous avons le regret de vous annoncer que la commande référence '.$sale->reference.' est annulée. L\'acheteur '.$buyer->login.' a été informé de celle-ci et sera remboursé de la somme de la transaction. A très vite, sur notre site. L\'équipe i-tou.';
        $message[1] = "Votre vente ".$sale->reference." n'a pas été expédiée à temps et est annulée Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message cancellation
     * sent to the buyer when the seller didn't send the package after 6 days
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellationAfterRelaunchSellerToSendPackageBuyer($buyer, $seller, $sale)
    {
        $message[0] = 'Votre achat « '.$sale->name.' », n\'a pas été expédié à temps par le vendeur '.$seller->login.'. Nous avons le regret de vous annoncer que la commande référence '.$sale->reference.' est annulée. Vous serez remboursé de la somme de '.$sale->price.'€ dans les 5 jours ouvrés. A très vite, sur notre site. L\'équipe i-tou.';
        $message[1] = "Votre achat ref ".$sale->reference." n'a pas été expédié par le vendeur et est annulé. Consultez vos emails/i-tou.fr";
        return $message;
    }

     /**
     * Message relaunch
     * sent to the buyer when he didn't pick up his package after 7 days
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messagePackageInRelais($buyer, $seller, $sale)
    {
        $message[0] = 'Vous avez récemment acheté l\'article «'.$sale->name.'» par le vendeur '.$seller->login.' sur notre site www.i-tou.fr d\'un montant de '.$sale->price.'€. Vous avez été informé par le transporteur de son arrivée en relais colis. Faite vite, il ne vous reste plus que 3 jours pour vous le réceptionner ! Passer ce délai, la commande référence '.$sale->reference.' sera annulée. A très vite, sur notre site www.i-tou.fr. L\'équipe i-tou';
        $message[1] = "Réceptionnez votre colis ".$sale->reference." il ne vous reste que 3 jours. Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message relaunch
     * sent to the buyer when he didn't pick up his package after 11 days
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellingPackageInRelaisBuyer($buyer, $seller, $sale)
    {
        $message[0] = 'Suite à l\'achat de l\'article «'.$sale->name.'» par le vendeur '.$seller->login.' sur notre site www.itou.fr d’un montant de '.$sale->price.'€, n\'a pas été réceptionné. La commande référence '.$sale->reference.', a été annulée, le colis est retourné au relais colis de départ. Le vendeur est informé de son retour. Nous procédons au remboursement de l’article (voir CGV). A très vite, sur notre site www.i-tou.fr. L\'équipe i-tou';
        $message[1] = "Votre achat ".$sale->reference." est annulé et retourné au vendeur, consultez vos emails / i-tou.fr";
        return $message;
    }
    
    /**
     * Message relaunch
     * sent to the seller when the buyer didn't pick up his package after 11 days
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancellingPackageInRelaisSeller($buyer, $seller, $sale)
    {
        $message[0] = 'Suite à la vente de votre article «'.$sale->name.'» par l’acheteur '.$buyer->login.' sur notre site www.i-tou.fr d’un montant de '.$sale->price.'€, nous vous informons que le colis n’a pas été réceptionné à temps, la commande référence '.$sale->reference.' a été annulée et votre colis vous est retourné dans votre relais colis de départ. Si vous aviez payé les frais de transport, ceux-ci vous seront restitué (voir CGV). A très vite, sur notre site www.i-tou.fr. L’équipe i-tou';
        $message[1] = "Votre vente ".$sale->reference." est annulée et retournée dans votre point relais, consultez vos emails / i-tou.fr";
        return $message;
    }

    /**
     * Message to inform the seller that he will be paid
     * sent to the seller 2 days after the buyer picked up his package
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messagePaySeller($buyer, $seller, $sale)
    {
        $message[0] = 'i-tou vous remercie pour votre commande référence «'.$sale->reference.'» effectuée sur notre site. Le colis a été réceptionné par l\'acheteur '.$buyer->login.'. La somme de '.$sale->price.'€ sera créditée sur votre compte bancaire dans les prochains jours. A très vite, sur notre site www.i-tou.fr. L\'équipe i-tou';
        return $message;
    }

    /**
     * Message to informe the buyer that he will be debited
     * sent to the buyer 1 day after he picked up his package
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageDebitBuyer($buyer, $seller, $sale)
    {
        $message[0] = 'i-tou vous remercie pour votre commande référence «'.$sale->reference.'» effectuée sur notre site. A très vite, sur notre site www.i-tou.fr. L\'équipe i-tou';
        return $message;
    }

    /**
     * Message to informe the buyer
     * that the transaction has been cancelled
     * by the seller (before he paid)
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancelProductBuyer($buyer, $seller, $sale)
    {
        $message[0] = 'Nous sommes désolés, la transaction pour votre achat de l’article '.$sale->name.', référence '.$sale->reference.', a été annulée par '.$seller->login.'. Pour en savoir plus, merci de le/la contacter directement. A bientôt sur www.i-tou.fr !';
        $message[1] = "Votre achat ".$sale->reference." a été annulé . Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message to informe the seller
     * that he cancelled the transaction
     * before the buyer paid
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancelProductSeller($buyer, $seller, $sale)
    {
        $message[0] = 'Vous avez effectué une demande de transaction pour expédier votre article '.$sale->name.', référence '.$sale->reference.', pour '.$buyer->login.'. Suite à votre demande d’annulation, nous vous informons que celle-ci a bien été prise en compte. Merci et à bientôt sur www.i-tou.fr !';
        $message[1] = "Votre vente ".$sale->reference." a été annulée. Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message to informe the seller
     * that he cancelled the order
     * after the buyer paid
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancelOrderSeller($buyer, $seller, $sale)
    {
        $message[0] = 'Vous avez effectué une demande de transaction pour expédier votre article '.$sale->name.', référence '.$sale->reference.', pour '.$buyer->login.'. Nous vous informons que celle-ci a été annulée. Merci et à bientôt sur www.i-tou.fr/nous-contacter !';
        $message[1] = "Votre vente ".$sale->reference." a été annulée. Plus d'info sur i-tou.fr";
        return $message;
    }

    /**
     * Message to informe the buyer
     * that the order has been cancelled
     * by the seller (after he paid)
     * @param buyer entity
     * @param seller entity
     * @param sale entity
    */
    public static function messageCancelOrderBuyer($buyer, $seller, $sale)
    {
        $message[0] = 'Nous sommes désolés, la transaction pour votre achat de l’article '.$sale->name.', référence « '.$sale->reference.' », a été annulée. Si vous avez déjà effectué le paiement, vous recevrez le remboursement dans les 5 jours ouvrés. A bientôt sur www.i-tou.fr/nous-contacter';
        $message[1] = "Votre achat ".$sale->reference." a été annulé. Plus d'info sur i-tou.fr";
        return $message;
    }

    public static function messageNoticeAdmin($sale)
    {
        $message[0] = 'Bonjour, Commande « '.$sale->reference.' » à rembourser. Bonne journée';
       // $message[1] = 'Bonjour, Commande « '.$sale->reference.' » à rembourser. Bonne journée'; //pas besoin de payer pour ca.
        return $message;
    }

}