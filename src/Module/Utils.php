<?php
namespace Modules\Itou\Itou\Module;
Use DbQuery;
Use Db;
Use Context;
Use OrderHistory;

trait Utils
{
    /**
     * Generate a random reference
     * @return string
     */
    public function randomRef() {
        $car = rand(5, 12);
        $length = 5;
		$string = "";
		$chaine = "AB1CD2EF3GH4IJ5KL6MN7OP8QR9ST0UV1WX2YZ9AB8CD7EF6GH5IJ4KL3MN2OP1QR0ST1UV2WX3YZ";
		srand((double) microtime() * 1000000);
		for ($i = 0; $i < $car; $i++) {
			$string .= $chaine[rand() % strlen($chaine)];
		}
		return  substr(md5($string.uniqid()),$car, $length);
    }
	
	 /**
     * Check if field is empty in database
     * @return bool
     */
    public function checkField($field, $table, $where, $where_value, $return_value = false, $test_date = false){

        if($test_date) {
            $test_date = 'AND (
                (c.`date_add` <= NOW() AND c.`date_end` >= NOW()) 
                OR 
                (c.`date_add` <= NOW() AND c.`date_end` = "0000-00-00 00:00:00")
            )';
        }

        $count = (new DbQuery())
        ->select($field)
        ->from($table, 'c')
        ->where('c.'.$where.' = "' . pSQL($where_value) . '"' . $test_date);

        if($result = Db::getInstance()->getRow($count)) {
           if(!is_null($result[$field]) && !empty($result[$field])){
                if($return_value) {
                    return $result[$field];
                } else {
                    return true;
                }
           }
        }
        return false;
    }

    /**
     * Remove taxes from Price
     * @param $price_with_taxes
    */
    public static function calculatePriceWT($price_with_taxes, $taxe_percentage)
    {
        $price_without_taxes = $price_with_taxes / (1 + ($taxe_percentage/100));
        return number_format($price_without_taxes, 6);
    }

    /**
     * Add taxes from Price
     * @param $price_without_taxes
    */
    public static function calculatePriceWithoutTaxes($price_without_taxes, $taxe_percentage)
    {
        $price_with_taxes = $price_without_taxes * (1 + ($taxe_percentage/100));
        return number_format($price_with_taxes, 6);
    }

    /**
     * @param $string
     */
    public function dotToComma($string){
		if($string != ""){
			return str_replace(".",",",$string);
		}
    }
    
    /**
     * Allow to add only one product in cart
     * called in hookActionCartSave if needed
     */
    public function oneProductInCart()
    {
        $cart = Context::getContext()->cart;
        if(!is_null($cart)) {
            $last = $cart->getLastProduct();
            $products_cart = $cart->getProducts();
         
            foreach ($products_cart as $product){
                if ($product['id_product'] != $last['id_product']){
                    $cart->deleteProduct($product['id_product'], '0');
                }else{
                    if($last['cart_quantity'] > 1){
                        $cart->deleteProduct($last['id_product'], '0');
                        $cart->updateQty(1, $last['id_product']);
                        return false;
                    }
                }
            }
        }
    }

    /**
     * @author Lucile
     * Change the order state 
     */
    public static function changeOrderState($order_id, $state_id)
    {
        $history = new OrderHistory();
        $history->id_order = $order_id;
        $history->changeIdOrderState($state_id, $order_id);
        $history->id_employee = 1;
        $history->save();
    }

     /**
     * @author Lucile
     * @param $string
     * @return mixed
     * Replace "33" by "0"
     */
    public static function formattedPhone($string)
    {
        return preg_replace("#^33([0-9]{9})$#", '0$1', $string);
    }

     /**
     * @author Lucile
     * @param $string
     * @return mixed
     * Replace "0" by "33"
     */
    public static function formattedPhoneSms($string)
    {
        return preg_replace("#^0([0-9]{9})$#", '33$1', $string);
    }

    /**
    * @author Lucile
    * @param $int
    * @param $int
    * @param $int
    * @param $string
    * Set Pagination to smarty
    */
    public static function paginate($nb_orders, $p, $n, $range = 4, $request_page = 'sales')
    {
        // to paginate, get the total order number 
        $pages_nb = ceil($nb_orders / (int)$n);
        $start = (int)($p - $range);
        if ($start < 1)
            $start = 1;
        $stop = (int)($p + $range);
        if ($stop > $pages_nb)
            $stop = (int)$pages_nb;

        if (!$p) $p = 1;

        Context::getContext()->smarty->assign(array(
            'pages_nb' => $pages_nb,
            'prev_p' => $p != 1 ? $p - 1 : 1,
            'next_p' => (int)$p + 1  > $pages_nb ? $pages_nb : $p + 1,
            'requestPage' => Context::getContext()->link->getPageLink('module-itou-'.$request_page),
            'p' => $p,
            'n' => $n,
            'range' => $range,
            'start' => $start,
            'stop' => $stop,
        ));
    }
}