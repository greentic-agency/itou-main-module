<?php

namespace Modules\Itou\Itou\Module;

use Configuration;
use AdminController;
use HelperForm;
use Tools;

trait Configurable
{
    protected $form = [
        'sections' => [],
        'values' => []
    ];
    public function getContent()
    {
        $process = $this->setConfigurationProcess();
        $this->setConfigurationPage();
        return $this->setConfigurationForm($process);
    }
    protected function setConfigurationProcess() {}
    protected function setConfigurationPage() {}
    protected function setConfigurationForm($process)
    {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->languages = $this->context->controller->getLanguages();
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'submit'.$this->name;
		$helper->toolbar_btn = array(
			'save' =>
			array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
				'&token='.Tools::getAdminTokenLite('AdminModules'),
			),
			'back' => array(
				'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
				'desc' => $this->l('Back to list')
			)
        );
        $helper->fields_value = $this->form['values'];
		return $process . $helper->generateForm($this->form['sections']);
    }
    public function setConfiguration($key, $value)
    {
        $configuration = $this->getConfiguration();
        $configuration[$key] = $value;
        Configuration::updateValue(
            sprintf('%s_SETTINGS', strtoupper($this->name)),
            json_encode($configuration)
        );
    }
    public function getConfiguration($key = null)
    {
        if (
            ($json = Configuration::get(sprintf('%s_SETTINGS', strtoupper($this->name))))
            && ($configuration = json_decode($json, true))
            && is_array($configuration)
        ) {
            if (!$key) {
                return $configuration;
            }
            if (isset($configuration[$key])) {
                return $configuration[$key];
            }
        }
        return $key ? null : [];
    }
}