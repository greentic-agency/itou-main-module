<?php

namespace Modules\Itou\Itou\Module;

Use Tools;
Use Cookie;
Use ImageType;
Use Image;
Use ImageManager;
Use Product;

trait Upload
{

    /**
	 * Upload image on server 
	 */
	public function uploadImage($context)
	{
        $uploadPath = _PS_MODULE_DIR_ . 'itou/views/uploads/';
		$path = $uploadPath . $context->cookie->id_connections;
		@mkdir($path, 0755, true);
		$upload_success = false;
   
            $filename = strtolower($_FILES['file']['name']);
			if(!file_exists($path."/".$filename)) {
                if($upload_success = move_uploaded_file($_FILES["file"]["tmp_name"],
                                     $path."/".$filename)) {
					$success_message = array( 'success' => 200,
						'filename' => $_FILES["file"]["tmp_name"],$path."/". $filename,
						'fileDir' => $path,
                    );
                }
			}

		if($upload_success) {
            $context->cookie->__set("uploadedFilePath", $path."/". $filename);
		}else{
            die(json_encode(array('error', 400)));
        }
    }
    
    /**
     * Save the image to the new product
     */
    public function saveImageToProduct($context, $id_product)
    {
        if($id_product && isset($context->cookie->uploadedFilePath)) {
            $old_path = $context->cookie->uploadedFilePath;
            $context->cookie->__unset('uploadedFilePath');
            $imagesTypes = ImageType::getImagesTypes('products');

            $image = new Image();
            $image->id_product = $id_product;
            $image->cover = true;
            $image->add();

            $new_path = $image->getPathForCreation();
            
            foreach ($imagesTypes as $image_type) {
                @ImageManager::resize($old_path, $new_path .'-'.$image_type['name'] . '.jpg', 
                                        $image_type['width'],
                                        $image_type['height']);
            }

            @ImageManager::resize($old_path, $new_path.'.jpg');

            unlink($old_path);
        }
    }

    /**
     * remove Uploaded Image
     * $image_id is null in case of first upload
     * if image is linked to a product, the image has an ID
     * @param $image_id
     */
    public function removeUploadedImage($context, $image_id = null)
    {
        if(is_null($image_id)) {
            if(file_exists($context->cookie->uploadedFilePath)) {
                unlink($context->cookie->uploadedFilePath);
            }
        }else{
            $image = new Image($image_id);
            $image->delete();
        }
    }
    
    /**
     * Test if folder exists
     */
    function folderExist($folder)
    {
		$path = realpath($folder);
		return ($path !== false AND is_dir($path)) ? $path : false;
    }
    
    /**
     * TODO 
     */
    function deleteEmptyFolder()
    {
        $src = _PS_MODULE_DIR_ . 'itou/views/uploads';

        $dir = opendir($src);

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    Upload::deleteEmptyFolder($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }

    /**
     * Get images by product ID - "echo" is for displaying image in dropzone
     * in forms creation / edition product of my account 
     * @params $product_id, $context
     */
    public function getUploadedImages($product_id = null, $context = null)
    {
        if(is_null($product_id)){
            $product_id = $context->cookie->productIdEdited;
            $context->cookie->__unset('productIdEdited');
        }

        if(is_null($context))
            $context = Context::getContext();

        $product = new Product($product_id);
        $images = $product->getImages($context->language->id);
        $type = 'home_default';
        $result = [];
    
        foreach ($images as $image) {
            $image_obj = new Image($image['id_image']);
            $uri_path = $image_obj->getPathForCreation() .
                        ($type ? '-' . $type : '') . '.jpg';

            $uri =  $_SERVER['HTTP_ORIGIN'] . _THEME_PROD_DIR_ . Image::getImgFolderStatic($image['id_image']) . $image['id_image'] . ($type ? '-' . $type : '') . '.jpg';


            if(file_exists($uri_path)) {
                $obj = [];
                $obj['name'] = $image['id_image'] .
                                ($type ? '-' . $type : '') . '.jpg';
                $obj['realPath'] = $uri;
                $obj['size'] = filesize($uri_path);
                $obj['id_image'] = $image['id_image'];
                $result[] = $obj;
            } else {
                $the_image = new Image($image['id_image']);
                $the_image->delete();
            }
         
        }

        echo json_encode($result);
    }
}