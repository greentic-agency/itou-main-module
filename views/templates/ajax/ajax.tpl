
{if $result}
    <input type='hidden' value='{$customer_id}' name='buyer_id'>
    <div class="alert alert-success" role="alert">
        <strong>{l s='Ok !'}</strong> {l s='Utilisateur trouvé !'}
    </div>
{else}
    <div class="alert alert-danger" role="alert">
        <strong>{l s='Oh mince !'}</strong> {l s='Utilisateur non trouvé !'}
    </div>
{/if}