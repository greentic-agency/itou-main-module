{extends file='customer/page.tpl'}
{block name='page_content'}

  {include file='module:itou/views/templates/front/my-account/forms/add-purchase.tpl'}
  {if !empty($purchases)}
    <ul>
      {foreach from=$purchases item=purchase name=purchases}
        {assign var='state' value={l s='En attente' mod='Modules.Itou'}}
        {if $purchase.order_id}
          {assign var='state' value={l s='Payé' mod='Modules.Itou'}}
        {elseif $purchase.date_send && $purchase.date_send != '0000-00-00 00:00:00'}
          {assign var='state' value={l s='Lien reçu le %s, en attente de paiement' sprintf=[$purchase.date_send|date_format:"%d/%m/%Y à %H:%M"]  mod='Modules.Itou'}}
        {/if}

        <li>
          {$purchase.reference} - {$purchase.name} - {$state} 
          {if !$purchase.order_id}
            <a href='{url entity='cart' params =['add' => true, 'qty' => 1, 'id_product' => $purchase.id, 'token' => $static_token]}' class='btn btn-info'>
              {l s='Payer' mod='Modules.Itou'} 
            </a>
          {/if}
        </li>
      {/foreach}
    </ul>
  {/if}
{/block}
