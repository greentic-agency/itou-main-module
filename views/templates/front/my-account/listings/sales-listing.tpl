{extends file='customer/page.tpl'}
{block name='page_content'}
    {if !empty($sales)}
        <ul>
        {foreach from=$sales item=sale name=sales}
            {assign var='state' value={l s='En attente' mod='Modules.Itou'}}
            {if $sale.order_id}
                {assign var='state' value={l s='Payé' mod='Modules.Itou'}}
            {elseif $sale.date_send && $sale.date_send != '0000-00-00 00:00:00'}
                {assign var='state' value={l s='Envoyé à %s le %s' sprintf=[$sale.buyer->login, $sale.date_send|date_format:"%d/%m/%Y à %H:%M"]  mod='Modules.Itou'}}
            {/if}

            <li>
            {if $sale.id_image}
                <img src='{$link->getImageLink($sale.link_rewrite, $sale.id_image, 'home_default', $sale.id_product)|escape:'html':'UTF-8'}' >
            {/if}
            
             {$sale.reference} - {$sale.name} - {$state}
                {* Générer mon étiquette *}
                {if $susbcription_customer == true}
                    {if !$sale.date_send || $sale.date_send == '0000-00-00 00:00:00'}
                        {if !empty($sale.buyer) && $sale.buyer->is_noticed_by_text_message == true && !is_null($sale.buyer->phone) }
                            <a href='{url entity='module' name='itou' controller='sendnotifications' params=['sendSms' => true, 'sale_id' => $sale.id_product]}' class='btn btn-info'>
                                {l s='Envoyer par' mod='Modules.Itou'} {l s='SMS' mod='Modules.Itou'} {l s='à' mod='Modules.Itou'} {$sale.buyer->login}
                            </a>
                        {elseif empty($sale.buyer)}
                            {l s='Acheteur non renseigné'}
                               <a href='{url entity='module' name='itou' controller='creationsale' params=['editProduct' => true, 'product_id' => $sale.id_product]}' class='btn btn-info'>
                                    {l s='Mettre à jour' mod='Modules.Itou'}
                                </a>
                        {else}
                            <a href='{url entity='module' name='itou' controller='sendnotifications' params=['sendEmail' => true, 'sale_id' => $sale.id_product ]}' class='btn btn-info'>{l s='Envoyer par' mod='Modules.Itou'} {l s='email' mod='Modules.Itou'}</a>
                        {/if}
                    {/if}
                {else}
                     <a href='{url entity='module' name='subscription' controller='listing'}' class='btn btn-info'>{l s='Choisir une formule' mod='Modules.Itou'}</a>
                     <a href='{url entity='module' name='itou' controller='creationsale' params=['editProduct' => true, 'product_id' => $sale.id_product]}' class='btn btn-info'>
                        {l s='Mettre à jour' mod='Modules.Itou'}
                    </a>
               {/if}
            </li>
        {/foreach}
       </ul>
    {/if}
{/block}
