<div id='message-subscription'>
    <div class='alert alert-danger'>
        {l s='Vous n\'avez pas encore choisi de formule,
            vous pourrez enregistrer votre transaction puis choisir une formule 
            avant de la valider totalement' Mod='Modules.Itou'}
    </div>
</div>