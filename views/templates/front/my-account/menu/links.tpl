<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="sales-link" href="{url entity='module' name='itou' controller='sales'}">
    <span class="link-item">
        <i class="material-icons">&#xE853;</i>
        {l s='Mes ventes' mod='Modules.Itou'}
    </span>
</a>
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="purchases-link" href="{url entity='module' name='itou' controller='purchases'}">
    <span class="link-item">
        <i class="material-icons">&#xE853;</i>
        {l s='Mes achats' mod='Modules.Itou'}
    </span>
</a>
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="purchacreationsaleses-link" href="{url entity='module' name='itou' controller='creationsale'}">
    <span class="link-item">
        <i class="material-icons">&#xE853;</i>
        {l s='Créer un transaction' mod='Modules.Itou'}
    </span>
</a>

{widget name="customerextrafields" }