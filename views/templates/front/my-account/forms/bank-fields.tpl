<div class="hidden-xl-down">
    <div id="bank-fields">
        {l s='Vos informations bancaires' mod='Modules.Itou'}
        <form action="" name="save-bank-field">
        <input type='hidden' name='customer_id' value=''>
            <div class="form-group">
                <label for="iban">{l s='IBAN' mod='Modules.Itou'}</label>
                <input id='iban' type='text' value='{$iban}' required>
            </div>
            <div class="form-group">
                <label for="bic">{l s='BIC' mod='Modules.Itou'}</label>
                <input id='bic' type='text' value='{$bic}' required>
            </div>
            <div class="form-group">
                <input id='send-bank-fields' type='submit' value='{l s='Enregistrer'}'>
            </div>
        </form>

        <div id='message-alert' class='hidden-xl-down'>
            <div class='alert alert-danger'>
                {l s='Veuillez remplir les 2 champs SVP.' Mod='Modules.Itou'}
            </div>
        </div>
    </div>
</div>