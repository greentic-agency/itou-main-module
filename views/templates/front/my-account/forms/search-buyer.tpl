<div class='form-group'>
    <label>{l s='Acheteur' mod='Modules.Itou'}</label>
    <input type='text' value='{if isset($buyer_login)}{$buyer_login}{/if}' id='buyer_name' name='buyer_name' ><button id='search-buyer' type="button" class="btn btn-info btn-sm">{l s='Rechercher' mod='Modules.Itou'}</button>
</div>

<div id='result-buyer'>
  <input type='hidden' value='{if isset($buyer_id)}{$buyer_id}{/if}' name='buyer_id'>
</div>
