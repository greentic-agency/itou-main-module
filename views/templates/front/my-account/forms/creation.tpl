{extends file='customer/page.tpl'}
{block name='page_content'}

     <form action='{$urls.base_url}{$url_upload}' class='dropzone' id='my-awesome-dropzone' enctype='multipart/form-data' method='post'>
        <div class='fallback'>
            <input name='file' type='file' />
        </div>
    </form>

    {if !$iban || !$bic}
       {include file='module:itou/views/templates/front/my-account/notifications/message-bank.tpl'}
    {/if}
    {if !$has_subscription}
        {include file='module:itou/views/templates/front/my-account/notifications/message-subscription.tpl'}
    {/if}

    <form action='{$link->getModuleLink('itou', 'creationsale')}' method='POST'>
        {include file='module:itou/views/templates/front/my-account/forms/search-buyer.tpl'}
        <div class='form-group'>
            <label>{l s='Nom' mod='Modules.Itou'}</label>
            <input type='text' value='' name='name' required >
        </div>
        <div class='form-group'>
            <label>{l s='Prix' mod='Modules.Itou'}</label>
            <input id='price' type='number' name='price' step='0.01' required >
        </div>
        <div class='form-group'>
            <label>{l s='Poids' mod='Modules.Itou'}</label>
            <input type='number' name='weight' value='' required >
        </div>
        <div class='form-group'>
            <input type='checkbox' name='cgv' value='1' required >
            <label>{l s='J\'accepte les conditions générales de vente' mod='Modules.Itou'}</label>
        </div>
        <div class='form-group'> 
            <label>{l s='Je veux prendre en charge les frais de port' mod='Modules.Itou'}</label>
            <label>{l s='Oui'}</label>
            <input type="radio" id='yes' name='is_shipping_cost_paid' value='1' />
            <label>{l s='Non'}</label>
            <input type="radio" id='no' name='is_shipping_cost_paid' value='0' />
        </div>
        <input type='submit' name='submit' value='{l s='Enregistrer'}'/>
    </form>
</div>

{include file='module:itou/views/templates/front/my-account/forms/bank-fields.tpl'}

{/block}
