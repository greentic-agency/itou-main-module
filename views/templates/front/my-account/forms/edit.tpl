{extends file='customer/page.tpl'}
{block name='page_content'}
     <form action='{$urls.base_url}{$url_upload}' class='dropzone' id='my-awesome-dropzone' enctype='multipart/form-data' method='post'>
        <div class='fallback'>
            <input name='file' type='file' >
        </div>
    </form>
    <hr/>

    {if !$iban || !$bic}
       {include file='module:itou/views/templates/front/my-account/notifications/message-bank.tpl'}
    {/if}

   <form action='{url entity='module' name='itou' controller='creationsale'}' method='POST'>
   <input type='hidden' name='product_id' value='{$product->id}'>
     {include file='module:itou/views/templates/front/my-account/forms/search-buyer.tpl'}
        <div class='form-group'>
            <label>{l s='Nom' mod='Modules.Itou'}</label>
            <input type='text' value='{$product->name}' name='name' required >
        </div>
        <div class='form-group'>
            <label>{l s='Prix' mod='Modules.Itou'}</label>
            <input id='price' type='number' name='price' value='{$product->price|round:2}' step='0.01' required >
        </div>
            <div class='form-group'>
            <label>{l s='Poids' mod='Modules.Itou'}</label>
            <input type='number' name='weight' value='{$product->weight|round:2}' required >
        </div>
        <div class='form-group'> 
            <label>{l s='Je veux prendre en charge les frais de port' mod='Modules.Itou'}</label>
            <label>{l s='Oui'}</label>
            <input type="radio" id='yes' name='is_shipping_cost_paid' value='1' {if $is_shipping_cost_paid == true}checked{/if} />
            <label>{l s='Non'}</label>
            <input type="radio" id='no' name='is_shipping_cost_paid' value='0' {if $is_shipping_cost_paid == false}checked{/if} />
        </div>
        <input type='submit' name='submit' value='{l s='Mettre à jour'}'/>
   </form>

   {include file='module:itou/views/templates/front/my-account/forms/bank-fields.tpl'}
{/block}
