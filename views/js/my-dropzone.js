$(document).ready(function () {
    var i = 0;
    var fileList = [];
    var script_remove = 'index.php?fc=module&module=itou&controller=creationsale&removeFile=1';
    var script_get =  'index.php?fc=module&module=itou&controller=creationsale&getFiles=1'
    Dropzone.options.myAwesomeDropzone = {

        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        dictCancelUpload: 'Annuler',
        dictRemoveFile: 'Supprimer',
        dictDefaultMessage : 'Ajouter une image',
        dictFileTooBig: 'La taille de l\'image est trop grande. Taille maximum: 10mb.',
        dictMaxFilesExceeded: 'Seulement 1 image autorisée par téléchargement.',
        acceptedFiles: '.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF',
        addRemoveLinks: true,
        
        init: function() {
         
            var thisDropzone = this;

            $.ajax({
                type: 'POST',
                headers: { "cache-control": "no-cache" },
                url: script_get,
                dataType: 'json',
                data: {
                    action : 'getUploadedImages',
                    ajax : true,
                },
                success: function(jsonData)
                {

                    $.each(jsonData, function(key,value){
                        var imgUrl = value.realPath;
                        var mockFile = {
                            name: value.name,
                            size: value.size,
                            accepted: true,
                            kind: 'image',
                            upload: {
                                filename: value.name,
                            },
                            dataURL: imgUrl,
                        };

                        thisDropzone.files.push(mockFile);
                        thisDropzone.emit("addedfile", mockFile);

                        thisDropzone.createThumbnailFromUrl(mockFile,
                            thisDropzone.options.thumbnailWidth,
                            thisDropzone.options.thumbnailHeight,
                            thisDropzone.options.thumbnailMethod,
                            true,
                            function(thumbnail) {
                                thisDropzone.emit('thumbnail', mockFile, thumbnail);
                                thisDropzone.emit("complete", mockFile);
                                thisDropzone.emit("success", mockFile);
                            }
                        );
                    });
                }
            });

            this.on("success", function (file, serverFileName) {
                var el = {
                    "fileDir": file.fileDir,
                    "fileName": file.name,
                    "fileId": i
                };
                $('.dz-message').show();
                i += 1;
                fileList.push(el);
            });
        },

        removedfile: function(file) {

            var name = file.name; 
            var image_id = (file.name).split("-");
                
            $.ajax({
             type: 'POST',
             url: script_remove,
             data: {name: name,request: 2, image_id: image_id[0]},
             sucess: function(data){
              console.log('success: ' + data);
             }
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
           }
    };


});