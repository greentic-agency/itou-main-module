/** If price is equal to zero seller cannot pay shipping cost */
$(document).on('keyup', '#price', function() {
    if(this.value == 0) {   
     $('input[name=is_shipping_cost_paid][value=0]').prop('checked', 'checked'); 
     $('input[name=is_shipping_cost_paid]').attr("disabled", true);
    }else{
         $('input[name=is_shipping_cost_paid]').attr("disabled", false);
    }
});