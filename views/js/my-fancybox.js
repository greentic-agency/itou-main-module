$(document).ready(function () {

    $('.fancybox').fancybox({
        href : "#bank-fields" 
    });

    $("#send-bank-fields").click(function(){
        var script_save = 'index.php?fc=module&module=itou&controller=creationsale&saveBankFields=1';
      
        bic = $('#bic').val();
        iban = $('#iban').val();

        if(bic != '' && iban != ''){

            if(isIban(iban)) {
                $.ajax({
                    url : script_save,
                    ajax : true,
                    data : {
                        bic : bic,
                        iban : iban
                    },
                    success : function(data){
                        $('#message-bank').html('<div class="alert alert-success" role="alert"> Ok ! </div>');
                        $("#message-bank").fadeOut(3000);
                        $.fancybox.close();
                    }
                });
            }

        }else{
            $('#message-alert').removeClass('hidden-xl-down');
        }
        
        return false;
    });
});

/**- 
 * code pays à 2 lettres 
 * La clé RIB, à 2 chiffres.
 * Le code banque à 5 chiffres; 
 * Le code guichet à 5 chiffres ;
 * Le numéro du compte à 11 chiffres ou lettres au maximum; 
 * La clé RIB, à 2 chiffres.
 */
function isIban(value) {
    return /^([a-zA-Z]{2}[0-9]{2}[0-9]{5}[0-9]{5}[a-zA-Z0-9]{0,11}[0-9]{2})?$/.test(value.toUpperCase());
}

