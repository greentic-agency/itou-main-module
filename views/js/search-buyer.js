$(document).ready(function () {
    $("#search-buyer").click(function() {
        var script_search = 'index.php?fc=module&module=itou&controller=ajax&searchBuyer=1';
        var login = $('#buyer_name').val();

        if (login != '') {
            $.ajax({
                url : script_search,
                data : {
                    login : login
                },
                dataType : 'html',
                success : function(html){
                     $('#result-buyer').html(html);
                     $('#error-empty-buyer').addClass('hidden-xl-down');
                }
            });
        }
        
        return false;
    });

    $("input[name=submit]").click(function() {
        id_buyer = $("input[name=buyer_id]").val();
        if (id_buyer == null || id_buyer == '') {
           $('#error-empty-buyer').removeClass('hidden-xl-down');
            return false;
        }
    });

});