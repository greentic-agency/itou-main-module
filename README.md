# Module général du site I-tou
> ATTENTION ! Installer auparavant les modules dépendants : customerextrafields, productextrafields, subscription, freesms, bankcharges pour un fonctionnement optimal.


### **Pour changer les urls**  :
> Paramètres de la boutique > Trafic et SEO > Modification des urls

*   module-itou-sales
*   module-itou-purchases
*   module-itou-creationsale

### **Liens dans le menu "mon compte"**
*   __Lien__ : /mon-compte
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\menu\links.tpl
>  __Widget formulaire de mise à jour du login - module "customerextrafields"__</br>
__Fichier TPL__ : \modules\customerextrafields\src\views\templates\widget\login-update.tpl
__Shortcode Smarty__ : {widget name="customerextrafields" }</br>
On peut bien évidemment le mettre partout


### **Creation de produit / edition de produit**
*   __Lien__ : /module/itou/creationsale
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\forms\creation.tpl
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\forms\edit.tpl

### **Formulaire de recherche d'acheteur**
*   __Lien__ : /module/itou/creationsale
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\forms\search-buyer.tpl

### **Listing des ventes**
*   __Lien__ : /module/itou/sales
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\listings\sales-listing.tpl

### **Listing des achats**
*   __Lien__ : /module/itou/purchases
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\listings\purchases-listing.tpl

### **Formulaire pour ajouter une vente par référence**
*   __Lien__ : /module/itou/purchases
*   __Fichier TPL__ : \modules\itou\views\templates\front\my-account\forms\add-purchase.tpl

