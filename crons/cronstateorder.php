<?php
@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);
include(dirname(__FILE__).'/../../../config/config.inc.php');

$token = Configuration::getGlobalValue('CRONJOBS_EXECUTION_TOKEN');
require_once '../../customerextrafields/customerextrafields.php';
require_once '../../productextrafields/productextrafields.php';
require_once '../src/Module/Service/Sms.php';
require_once '../src/Service/SMS/SMSService.php';
require_once '../src/Service/SMS/Adapter/Exception/AdapterException.php';
require_once '../src/Service/SMS/Adapter/Interfaces/AdapterInterface.php';
require_once '../src/Service/SMS/Adapter/NexmoAdapter.php';
require_once '../src/Module/Service/Message.php';
require_once '../src/Service/SMS/Adapter/Exception/RejectedAdapterException.php';
require_once '../src/Module/Service/Email.php';
require_once '../src/Module/Utils.php';
require_once '../vendor/autoload.php';
/*

require_once '../vendor/nexmo/client/src/Client.php';
require_once '../vendor/nexmo/client/src/Client/Credentials/Basic.php';
require_once '../vendor/nexmo/client/src/Client/Credentials/AbstractCredentials.php';
*/
//c5187c1c62aeffe950643b0b2a71d426

if(Tools::getIsset('checkOrdersToPay') && Tools::getValue('token') == $token) {
    checkOrdersToPay();
} elseif(Tools::getIsset('checkOrdersPackageNotSend') && Tools::getValue('token') == $token) {
    checkOrdersPackageNotSend();
    checkOrdersPackageNotSendToCancel();
} elseif(Tools::getIsset('checkOrdersToRelaunch') && Tools::getValue('token') == $token) {
    checkOrdersToRelaunch();
} elseif(Tools::getIsset('checkOrdersPackageInRelais') && Tools::getValue('token') == $token){
    checkOrdersPackageInRelais();
}

/**
 * @author Lucile
 * Check if sellers of delivered / finished orders has to be refund 
 * after 48 hours
 */
function checkOrdersToPay()
{ 
    $orders_sql = (new DbQuery())
        ->select('id_order, delivery_date')
        ->from('orders', 'o')
        ->where('o.current_state = ' . Configuration::get('STATE_ORDER_DELIVERED')); // livré

    if($orders = Db::getInstance()->ExecuteS($orders_sql)) {
        foreach($orders as $order) {
            $diffDays = diffDays($order['delivery_date'], date("Y-m-d H:i:s"));
            $id_product = getProductsOrder($order['id_order']);
            if($diffDays == 1) {
                /** UPDATE ORDER STATUS TO "SELLER TO REFUND" ID 26 */
                changeOrderState($order['id_order'], Configuration::get('ITOU_STATE_BUYER_WILL_BE_PAID'));

                /** SEND SMS TO BUYER */
                // $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                // $sms->sendSms($id_product, 'debitBuyer');
                
                /** SEND EMAIL TO BUYER */
                // $email = new \Modules\Itou\Itou\Module\Service\Email();
                // $email->sendEmail($id_product, 'debitBuyer');
            }
        }
    }

    $orders_sql = (new DbQuery())
    ->select('id_order, delivery_date')
    ->from('orders', 'o')
    ->where('o.current_state = ' . Configuration::get('ITOU_STATE_BUYER_WILL_BE_PAID')); // livré

    if($orders = Db::getInstance()->ExecuteS($orders_sql)) {
        foreach($orders as $order) {
            $diffDays = diffDays($order['delivery_date'], date("Y-m-d H:i:s"));
            $id_product = getProductsOrder($order['id_order']);

            if($diffDays == 2) {
                /** UPDATE ORDER STATUS TO "SELLER TO REFUND" ID 26 */
                changeOrderState($order['id_order'], Configuration::get('ITOU_STATE_REFUND_SELLER'));

                /** SEND SMS TO SELLER */
                // $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                // $sms->sendSms($id_product, 'paySeller');
                
                /** SEND EMAIL TO SELLER */
             
                // $email = new \Modules\Itou\Itou\Module\Service\Email();
                // $email->sendEmail($id_product, 'paySeller');
            }
        }
    }
}

/**
 * @author Lucile
 * Check if seller has send the package
 * After 3 days until label relais colis has been sent by email
 */
function checkOrdersPackageNotSend()
{
    $order_state_label_sent = Configuration::get('ITOU_STATE_LABEL_SENT');
    $orders_sql = (new DbQuery())
    ->select('oh.id_order, oh.date_add AS date_label_sent, o.current_state')
    ->from('orders', 'o')
    ->innerJoin('order_history', 'oh', 'o.id_order = oh.id_order')
    ->where('o.current_state = ' . $order_state_label_sent)
    ->where('oh.id_order_state = ' . $order_state_label_sent);// Etiquette envoyée par email

    if($orders = Db::getInstance()->ExecuteS($orders_sql)) {
        foreach($orders as $order) {
            $id_product = getProductsOrder($order['id_order']);
            if(diffDays($order['date_label_sent'], date("Y-m-d H:i:s")) == 3) {
                /** UPDATE ORDER STATUS TO "PACKAGE NOT SEND YET" ID 27 */ 
                changeOrderState($order['id_order'], Configuration::get('ITOU_STATE_NOT_SENT'));
                /** SEND SMS AND EMAIL ONLY TO THE SELLER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($id_product, 'relaunchSellerToSendPackage');
                
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail($id_product, 'relaunchSellerToSendPackage');

            }
        }
    }
}

/**
 * @author Lucile
 * Check if seller has send the package
 * After 6 days until label relais colis has been sent by email
 * And cancel it
 */
function checkOrdersPackageNotSendToCancel()
{
    $order_state_not_sent = Configuration::get('ITOU_STATE_NOT_SENT');
    $orders_sql = (new DbQuery())
    ->select('oh.id_order, oh.date_add AS date_relance, o.current_state')
    ->from('orders', 'o')
    ->innerJoin('order_history', 'oh', 'o.id_order = oh.id_order')
    ->where('o.current_state = ' . $order_state_not_sent)
    ->where('oh.id_order_state = ' . $order_state_not_sent);//Colis non envoyé

    if($orders = Db::getInstance()->ExecuteS($orders_sql)) {
     
        foreach($orders as $order) {
            $id_product = getProductsOrder($order['id_order']);
         
            if(diffDays($order['date_relance'], date("Y-m-d H:i:s")) == 3) {
                /**  UPDATE ORDER STATUS TO "CANCEL" ID 6 */
                changeOrderState($order['id_order'], Configuration::get('STATE_ORDER_CANCELED'));

                /** CANCELLING ORDER IN PRODUCTEXTRAFIELDS */
                cancelProductExtraField($id_product);

                /**  SEND SMS TO THE SELLER AND THE BUYER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($id_product, 'cancellationAfterRelaunchSellerToSendPackage');
                    
                /**  SEND EMAIL TO THE SELLER AND THE BUYER */
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail($id_product, 'cancellationAfterRelaunchSellerToSendPackageBuyer');
                $email->sendEmail($id_product, 'cancellationAfterRelaunchSellerToSendPackageSeller');

                /** SEND SMS AND EMAIL TO ADMIN TO NOTICE TO REFUND THE BUYER OR SELLER */
                $sms->sendSms($id_product, 'noticeAdminCancel');
                $email->sendEmail($id_product, 'noticeAdminCancel');
            }
        }
    }
}

/**
 * @author Lucile
 * If buyer doesn't pay his order under 24 hours
 * depending on transaction's date send
 */
function checkOrdersToRelaunch()
{
    $productextrafields_sql = (new DbQuery())
    ->select('*')
    ->from('productextrafields', 'pef')
    ->where('pef.order_id = 0')
    ->where('pef.date_send IS NOT NULL')
    ->where('pef.date_send <> \'0000-00-00 00:00:00\'')
    ->where('pef.is_cancelled = 0 OR pef.is_cancelled IS NULL');

 
    if($productextrafields = Db::getInstance()->ExecuteS($productextrafields_sql)) {
        foreach($productextrafields as $productextrafield) {
            $diffDays = diffDays($productextrafield['date_send'], date("Y-m-d H:i:s"));
       
            if($diffDays == 1 
                && (is_null($productextrafield['date_relaunch'])
                || $productextrafield['date_relaunch'] == '0000-00-00 00:00:00')) {
               
                /** Update the date of relaunch */
                updateRelaunchProductExtraField($productextrafield['id']);

                /** SEND SMS TO BUYER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($productextrafield['id'], 'relaunch');
    
                 /** SEND EMAIL TO BUYER */
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail($productextrafield['id'], 'relaunch');

            } elseif(diffDays($productextrafield['date_relaunch'], date("Y-m-d H:i:s")) == 1) {
                /** Cancel the sale (order not created yet so we cannot update any order status) */
                cancelProductExtraField($productextrafield['id']);
               
                 /** SEND SMS TO BUYER  AND SELLER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($productextrafield['id'], 'cancellationAfterRelaunch');

                 /** SEND EMAIL TO BUYER  AND SELLER */
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail($productextrafield['id'], 'cancellationAfterRelaunchBuyer');
                $email->sendEmail($productextrafield['id'], 'cancellationAfterRelaunchSeller');
            }
        }

    }
}

/**
 * @author Lucile
 * If buyer didn't get his package in relais after 7 days and 11 days
 * depending on state 18 RC - Livraison en cours
 */
function checkOrdersPackageInRelais()
{
    //$order_state_delivery_process = Configuration::get('RC_STATE_LEC');
    $order_state_delivery_process = Configuration::get('RC_STATE_DER'); // déposé en relais = last step
   
    $ordersInRelais = (new DbQuery())
    ->select('oh.id_order, oh.date_add AS date_depot')
    ->from('orders', 'o')
    ->innerJoin('order_history', 'oh', 'o.id_order = oh.id_order')
    ->where('o.current_state = ' . $order_state_delivery_process)
    ->where('oh.id_order_state = ' . $order_state_delivery_process)
    ->orderby('oh.date_add');

    if($orders = Db::getInstance()->ExecuteS($ordersInRelais)) {
        foreach($orders as $order) {
            $diffDays =  diffDays($order['date_depot'], date("Y-m-d H:i:s"));
            $id_product = getProductsOrder($order['id_order']);

            if($diffDays == 7) {
                /** SEND SMS TO BUYER AND SELLER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($id_product, 'packageInRelais');

                /** SEND EMAIL TO BUYER AND SELLER */
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail($id_product, 'packageInRelais');

                changeOrderState($order['id_order'], Configuration::get('ITOU_STATE_FIRST_BUYER_PACKAGE'));
            }
        }
    }

    $order_state_delivery_process = Configuration::get('ITOU_STATE_FIRST_BUYER_PACKAGE');
   
    $ordersInRelais = (new DbQuery())
    ->select('oh.id_order, oh.date_add AS date_depot')
    ->from('orders', 'o')
    ->innerJoin('order_history', 'oh', 'o.id_order = oh.id_order')
    ->where('o.current_state = ' . $order_state_delivery_process)
    ->where('oh.id_order_state = ' . $order_state_delivery_process)
    ->orderby('oh.date_add');

    if($orders = Db::getInstance()->ExecuteS($ordersInRelais)) {
        foreach($orders as $order) {
            $diffDays =  diffDays($order['date_depot'], date("Y-m-d H:i:s"));
            $id_product = getProductsOrder($order['id_order']);
            if($diffDays == 4) {

                /** CANCELLING ORDER IN PRODUCTEXTRAFIELDS */
                cancelProductExtraField($id_product);

                /**  UPDATE ORDER STATUS TO "CANCEL" ID 6 */
                changeOrderState($order['id_order'], Configuration::get('STATE_ORDER_CANCELED'));

                /** SEND SMS TO BUYER AND SELLER */
                $sms = new \Modules\Itou\Itou\Module\Service\Sms();
                $sms->sendSms($id_product, 'cancellingAfterpackageInRelais');

                 /** SEND EMAIL TO BUYER AND SELLER */
                $email = new \Modules\Itou\Itou\Module\Service\Email();
                $email->sendEmail( $id_product, 'cancellingPackageInRelaisBuyer');
                $email->sendEmail( $id_product, 'cancellingPackageInRelaisSeller');

                /** SEND SMS AND EMAIL TO ADMIN TO NOTICE TO REFUND THE BUYER OR SELLER */
                $sms->sendSms($id_product, 'noticeAdminCancel');
                $email->sendEmail($id_product, 'noticeAdminCancel');
            }
        }
    }
}

/**
 * @author Lucile
 * Calcul the difference of days between two dates
 */
function diffDays($start, $end) {
    $start = new DateTime($start);
    $days = $start->diff(new DateTime($end));
    return $days->days;
}

/**
 * @author Lucile
 * Get the product created by the seller
 * for the given order ID to get all the data we wanted
 * For the buyer / seller etc
 * We excluded products bankcharges and subscriptions
 * @param int id_order
 * @return int id_product
 */
function getProductsOrder($id_order)
{
    $order = new Order($id_order);
    $id_product = false;
    $products = $order->getProducts();
    $productsToExclude =  array_values(Configuration::getMultiple(['SUBSCRIPTION_SIMPLE_ID',
                                                      'BANK_CHARGES_ID',
                                                      'SUBSCRIPTION_YEARLY_ID',
                                                      'SUBSCRIPTION_MONTHLY_ID']));
    if(empty($productsToExclude)) {
        throw new \Exception("No subscription id or bankcharges id found on table ps_configuration. \itou\src\Module\Service\cronstateorder.php function getProductsOrder(id_order)");
    }

    foreach($products as $product) {
        if(!in_array($product['product_id'], $productsToExclude)) {
            $id_product = $product['product_id'];
        }
    }
    return $id_product;
}

/**
 * @author Lucile
 * Change the order state 
 */
function changeOrderState($order_id, $state_id)
{
    $history = new OrderHistory();
    $history->id_order = $order_id;
    $history->changeIdOrderState($state_id, $order_id);
    $history->id_employee = 1;
    $history->save();
}

/**
 * @author Lucile
 * Set is_cancel to true to cancel product
 * In case order is not created
 */
function cancelProductExtraField($id) {
    if(!Db::getInstance()->execute('
    UPDATE `'._DB_PREFIX_.'productextrafields`
    SET `is_cancelled` = 1,`date_cancel` = "'.date("Y-m-d H:i:s").'"
    WHERE `id` = '.(int)$id
    )) {
        throw new \Exception("Update cancelling sale ".$id." failed. \itou\src\Module\Service\cronstateorder.php function cancelProductExtraField(id) ");
    }
}
    /**
 * @author Lucile
 * Set relaunch date
 * In case order is not created
 */
function updateRelaunchProductExtraField($id) {
    if(!Db::getInstance()->execute('
    UPDATE `'._DB_PREFIX_.'productextrafields`
    SET `date_relaunch` = "'.date("Y-m-d H:i:s").'"
    WHERE `id` = '.(int)$id
    )) {
        throw new \Exception("Update relaunch date for sale ".$id." failed. \itou\src\Module\Service\cronstateorder.php function updateRelaunchProductExtraField(id) ");
    }
}