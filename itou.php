<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . '/autoload.php';

class Itou extends Modules\Itou\Itou\Module
{
    public function __construct()
    {
        $this->name = 'itou';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Lucile Greentic';
        $this->bootstrap = true;
        $this->displayName = $this->l('I-tou');
        $this->description = $this->l('Gateway to sell and buy safely personal products ');
        parent::__construct();
        self::getInstanceByName('customerextrafields');
        self::getInstanceByName('productextrafields');
        self::getInstanceByName('subscription');
    }
}